# COMP3931-Individual-Project-Learning-to-Texture-3D-Models

# Requirements
- Python 3.8
- Pytorch with CUDA toolkit - installed via conda (for CUDA 11 use conda install pytorch torchvision torchaudio cudatoolkit=11.1 -c pytorch -c nvidia) for other versions of CUDA or to install via pip or from source see https://pytorch.org/
- A graphics card that supports CUDA
- Qt 5
- OpenGL version >= 2.4
 

# How to run the application

Download the zip file from gitlab and extract it\
cd ./COMP3931-Individual-Project-Learning-to-Texture-3D-Models/TextureGAN/src\
qmake\
make clean\
make\
./TextureGAN\
(note the application must be launched from the src folder. Please always cd into ./COMP3931-Individual-Project-Learning-to-Texture-3D-Models/TextureGAN/src before launching)

# Using the application
To use the application lauch it following the instructions above. Select a semantic colour and model you would like to texture. Paint the UV map on the left hand side of the application. Choose which GAN model you would like to use and add padding to the texture if necessary. Tick the styling option if you would like to add style transfer and enter a valid file image location into the textbox. Set the style impact and click the render button.

# Resources used in this repo

[BicycleGAN](https://github.com/junyanz/BicycleGAN) \
[Pytorch CycleGAN and pix2pix](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix) \
[Neural Style Tutorial](https://github.com/pytorch/tutorials/blob/master/advanced_source/neural_style_tutorial.py) \
[Describable Textures Dataset (DTD)](https://www.robots.ox.ac.uk/~vgg/data/dtd/index.html#overview) \
[Flickr Material Database (FMD)](https://people.csail.mit.edu/lavanya/fmd.html) 
