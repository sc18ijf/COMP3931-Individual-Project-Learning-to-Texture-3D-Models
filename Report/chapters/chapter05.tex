\chapter{GAN Training and Testing}
\label{ch:5pix2pixtrainingandtesting}


\section{Pix2pix Architecture}
\label{sec:pix2pixarchitecture}
\begin{figure}[H]
    \label{fig:realvsbicycleganvspix2pixbarchart}
    \includegraphics[scale=0.3,center]{./Images/pix2pixarch5.png}
    \caption{The Pix2Pix training architecture. The image is based on an image found in the "Image-to-Image Translation with Conditional Adversarial Networks, 2018" paper. \cite{isola2018imagetoimage}}
\end{figure}


The semantic segmentation UV map, $x$, is fed into the generator to produce $G(x)$ (the generated image). $G(x)$ and $x$ are then fed into the discriminator, $D$, which aims to classify $G(x)$ as fake and $y$ as real given $x$ as the condition. 

The discriminator is a deep convolutional neural network, referred to as PatchGAN. \cite{li2016precomputedpatchgan} The model is applied to $N$ x $N$ patches of the target image (either $G(x)$ or $y$) meaning the discriminator model can be applied to images of varying size. In the paper, it was found that 70 x 70 pixel patches performed best for their particular uses. \cite{isola2018imagetoimage}

\begin{figure}[H]
    \label{fig:encodedecode}
    \includegraphics[scale=0.4,center]{./Images/encoder-decoder.png}
    \caption{An encoder-decoder network compared to a network with U-Net architecture. \cite{isola2018imagetoimage}}
\end{figure}

The generator is an encoder-decoder convolutional neural network that can opt to use a U-Net architecture. The encoder takes an input and expresses it into a compressed form with a lower-dimensional latent space. This compressed version is then passed to the decoder which aims to reconstruct high dimensional features based on features found within the latent space. When using the U-Net architecture connections can be skipped meaning that features may not need to be encoded into the lowest dimensional form.

The generator trained via adversarial loss against the discriminator. The L1 and cGAN loss functions encourage the generator to generator output that better reflects the training set. The generator is deterministic meaning a given input $x$ will always map to the same output $G(x)$.



\section{BicycleGAN Architecture}
\label{sec:bicycleganarchitecture}
\label{subsubsec:studysection2}

\begin{figure}[H]
    \label{fig:pix2pixwithnoise}
    \includegraphics[scale=0.4,center]{./Images/pix2pixwithnoise.png}
    \caption{The Pix2Pix training architecture that is modified to add noise. \cite{towardmultimodalimagetoimagetranslationbicyclegan2017}}
\end{figure}

In the BicycleGAN paper, the architecture was modified so that the generator could also accept noise. The input image, $A$, is fed into a generator and $\widehat{B}$ is produced. $\widehat{B}$ is the generated, or fake, version of $B$ (the corresponding real image). $B$ and $\widehat{B}$ are then fed into the discriminator which decides if $\widehat{B}$ is real or fake. J.-Y. Zhu et al found that even with the addition of noise ($N(z)$) generated images lacked much variety as a result of mode collapse and the generator giving little significance to the noise. \cite{towardmultimodalimagetoimagetranslationbicyclegan2017}

\begin{figure}[H]
    \label{fig:bicycleGANnetwork}
    \includegraphics[scale=0.25,center]{./Images/bicycleGANnetwork.jpg}
    \caption{The BicycleGAN training architecture as described in the paper. \cite{towardmultimodalimagetoimagetranslationbicyclegan2017}}
\end{figure}

To deal with the issue of mode collapse the BicycleGAN architecture was developed. The BicycleGAN model contains a Conditional Variational Autoencoder-GAN (cVAE-GAN) that is shown on the left of \ref{fig:bicycleGANnetwork}and Conditional Latent Regressor GAN (cLR-GAN) that is shown on the right of \ref{fig:bicycleGANnetwork}. 

The cVAE-GAN uses an encoder to encode the ground truth into a latent space ($B \rightarrow z$). The input image ($A$) and then encoded ground truth is passed to a generator that creates a new fake image ($z \rightarrow \widehat{B}$). The ground truth image and the generated image are then passed to a discriminator. A KL loss is also introduced to provided consistency in the latent space.


The cLR-GAN uses a noise vector which is fed as an input to a generator along with the input image to create a new image ($z \rightarrow \widehat{B}$). The synthesised and ground-truth images are fed to a discriminator. The synthesised image is also passed to an encoder that aims to reconstruct the latent vector through the $L_1$ loss ($\widehat{B} \rightarrow \widehat{z}$).

The two models are used in tandem meaning the BicycleGAN has two generators and two discriminators. This makes it more memory intensive than the Pix2Pix model however, $z$ can be changed at test time to produce variety in its outputs given one input image. 

\section{Methodology}
\label{sec:methodology}
For the Pix2Pix model, the training and testing was performed in a Jupiter Notebook file. An anaconda environment was used to make sure all the various libraries that were needed for training and testing were present. For the final model used in the application the training parameters used were:

\begin{verbatim}
!python train.py --dataroot ./datasets/model9 --name model8 --model pix2pix
--direction BtoA --n_epochs 700  --display_id 0 --load_size 512 
--n_layers_D 5 --save_epoch_freq 25 --beta1 0.3 --lambda_L1 25  --crop_size 256 
\end{verbatim}

The train.py file uses a parser to set the training options. The dataset containing 6000 paired images was set using the --dataroot option. The --model option refers to if the Pix2Pix or CycleGAN model should be used. The models was trained for 700 epochs and took a total of 46 hours to train on a NVidia 3070 RTX. A Visdom interface could be used to visualise the loss logs using a GUI, however this was disabled using --display\_id 0 as it was causing errors. --n\_layers\_D refers to the number of layers used in the discriminator and corresponds with the size of the PatchGAN. The model was saved every 25 epochs so the model could be restored from a file if there were any interruptions in training. Beta was set to 0.3 in contrast to the default 0.5. The ==lambda\_L1 was set to 25 compared to the default 100. The lambda L1 option refers to the ratio of L1 to cGAN loss functions. We found that the default ratio of 100 was too favourable towards the L1 loss and produced blurry output images. The crop size was set to half of the load size. Additional information about the training settings can be found in the train\_opt.txt file that is placed alongside the Pix2Pix model.

For the BicycleGAN model a shell script was used to set the model options. The training took place within an anaconda environment using a linux terminal. The training parameters used were:

\begin{verbatim}
python ./train.py --display_id 1 --dataroot ./datasets/model9 --name model9
--model bicycle_gan --direction BtoA --checkpoints_dir ./checkpoints/model9 
--load_size 512 --crop_size 256 --nz 8 --input_nc 3 --niter 650 --use_dropout
--save_epoch_freq 25
\end{verbatim}

Since both repositories were maintained by the same individual many of the settings are named similarly. The --input\_nc setting refers to the number of input channels which is set to 3 (RGB). The model was trained for 650 epoch meaning it took a total of 65 hours to train.



\section{Experimenting with Training Parameters}
\label{sec:trainingparameters}
\subsection{Pix2Pix}
\label{subsec:pix2pixex}
\subsubsection{L1 and cGAN}
\label{subsubsec:l1andcgan}
\begin{figure}[H]
    \label{fig:lambda}
    \includegraphics[scale=0.35,center]{./Images/lambda_compare.png}
    \caption{Left: the generated output of a stone and foliage texture with --lambda\_l1 set to 100 after 635 epoch of training. Right: the generated output of a stone texture with --lambda\_l1 set to 25 after 629 epoch of training.
    }
\end{figure}

The image above demonstrates how the ratio of the $L_1$ to cGAN impacts the generated output. The $L_1$ uses an average causing the resultant images to be blurrier than images generated with a higher ratio of cGAN. While the image on the left's colours are desired, most of the stone texture information is lost to this averaging. When the cGAN loss is increased the roughness of the stone texture can be seen.


\subsubsection{Beta}
\label{subsubsec:beta}
Beta is the value of beta that is used during gradient descent. It refers to the learning rate of the model and can be thought of as the size of the step taken during gradient descent. When a smaller beta of 0.3 was used, models they had to be trained to longer to reach the same level of realism that models that used a beta of 0.5, however it can be agrued that models that used a smaller beta were able to learn the patterns in the textures better since they were less likely to overshoot global minima during gradient descent.


\subsubsection{Crop Size and Load Size}
\label{subsubsec:cropsize}
\begin{figure}[H]
    \label{fig:cropsize}
    \includegraphics[scale=0.35,center]{./Images/crop.png}
    \caption{An example of the two training image from the dataset and their cropped counterparts.}
\end{figure}
Initially, a load size of 256 was used on images that were 256, and the images were not cropped. After seeing that the model could perform on this smaller resolution training set a new training set was created where the input and ground truth images were of 512 x 512 resolution. The crop size was then changed to 256 so that during training a randomly sampled 256 x 256 patch of the image was used. This provided drastically more training examples from the same dataset.


\subsection{BicycleGAN}
\label{subsec:bicycleganex}
\subsubsection{Beta}
\label{subsubsec:betabicycle}
The default beta of 0.5 was used during the training of the BicycleGAN model. The value is a good compromise between a high enough learning rate that training time is reduced but is low enough so that steps within the gradient descent function are small enough so that patterns relationships between image $A$ and $B$ are still learned.

\subsubsection{Crop Size and Load Size}
\label{subsubsec:cropsizebicycle}
At this stage of the project, there was only enough time to train one BicycleGAN model. A load size of 512 x 512 was used to accept images of 512 x 512 pixels and a crop size of 256 x 256 pixels was used to increase the number of training examples. Generally speaking the options used for the BicycleGAN model were chosen based on what was learnt during training of the Pix2Pix model.



\section{Time}
\label{sec:time}

\begin{figure}[H]
    \label{fig:section1test}
\begin{center}
\begin{tabular}{ |c|c|c| } 
 \hline
  \thead{Number of Examples \\ in training set}           & 	\thead{Time taken to complete\\ one epoch (Pix2Pix)} &	\thead{Time taken to complete \\one epoch (BicycleGAN)}		 \\ \hline
  200   &       8 seconds   &     N/A           \\ \hline
  1200  &      44 seconds   &     N/A           \\ \hline
  6000  &     236 seconds   &     358 seconds   \\ \hline
\end{tabular}
\end{center}
    \caption{The time taken to train on epoch for datasets of various sizes using an Nvidia RTX 3070.}
\end{figure}

Within each epoch all of the training set examples are iterated through, so the time taken to carry out one epoch of training increases linearly with the training set size. One epoch of training takes longer for the BicycleGAN model than the Pix2Pix model with a dataset of the same size. This is due to the increased complexity of BicycleGAN compared to Pix2Pix. 