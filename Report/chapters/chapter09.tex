\chapter{Evaluation}
\label{ch:9resultstestingandevaluation}

\section{Evaluation}
\label{sec:testing}
\subsection{Fréchet Inception Distance}
\label{subsec:frechetinceptiondistance}
To test the similarity between the real data sets and the fake, generated ones the Fréchet Inception Distance (FID) was calculated. The Fréchet Inception Distance was introduced in 2017 \cite{fid2017} and is often used to calculate the performance of GANs\cite{lucicgansfideval2018}. The Fréchet Inception Distance is given by the formula:

\begin{center}
$d^2((m, C),(m_w, C_w)) = \vert \vert m - m_w \vert \vert_2^2 + Tr\big(C + C_w - 2(CC_w)^{1/2}\big)$
\end{center}

Where $m$ is the mean and $C$ is the covariance matrix of each of the multidimensional Gaussian distributions. With $(m_w, C_w)$ referring to the generated images. Tr is the trace linear algebra operation.

The FID uses a neural network and evaluates the similarity between the mean and covariance of the images at a deep level. The Inception v3 model used by the FID calculator is trained on the ImageNet \cite{deng2009imagenet}. A lower FID implies that there are more similarities between the images. 

For this project, a Pytorch Implementation of the FID was used \cite{Seitzer2020FID}. A test data set of 300 UV maps was created and examples were produced by the Pix2Pix and BicycleGAN models created. The real values were then compared to the generated values using the FID. The results are shown in the table below: 

\medskip

%bicyclegan fid 192.05382160427195
%pix2pix fid 254.25115281369813

\begin{figure}[H]
    \label{fig:section1testt}
    \begin{center}
        \begin{tabular}{ |c|c|c| } 
        \hline
                &  BicycleGAN    &	     Pix2Pix   \\ \hline
        FID     &      192.054   &       254.251   \\
        \hline
        \end{tabular}
        \caption{The Fréchet Inception Distance for the BicycleGAN and Pix2Pix models.}
    \end{center}
\end{figure}

The BicycleGAN model outperformed the Pix2Pix model implying that it was more similar to the real data set however, both models still score very highly. This shows that the models were not deemed similar to the test dataset of real examples.

We would expect this to be true since the original real images contained unwanted elements that we did not want to produce in our generated textures (such as white backgrounds). The models performed well at picking out similar features between classes, however this would not be reflected in a low FID score.
\newpage



\subsection{User Study}
\label{subsec:userstudy}
A survey was created using \href{https://forms.gle/3K5EnEzR5zDWAFbe8}{Google Forms} and 14 individuals were invited to participate. Of the 14 individuals, 10 people responded. The survey was divided into two sections. 


\subsubsection{Section 1}
\label{subsubsec:studysection1}
\begin{figure}[H]
    \label{fig:section1testimage}
\includegraphics[scale=0.5,center]{./Images/section1test.png}
    \caption{The 12 textured models participants were shown and asked to classify as being textured using the data set of texture or using the AI models.}
\end{figure}
In the first section participants were shown four different 3D models that were textured using both real and generated textures. Participants were then asked to classify each textured model as real (textured using the dataset of textures) or fake (generated by one of the artificial intelligence models). For each model, the number of times participants responded with 'real' is documented in the table below:

\begin{figure}[H]
    \label{fig:section1testtable}
\begin{center}
\begin{tabular}{ |c|c|c|c|c||c| } 
 \hline
             & 	Model 1 &	Model 2	& Model 3 & Model 4 & Average \\ \hline
  Real       &      8   &       5   &   4     &     5   &  5.5    \\ \hline
  BicycleGAN &      0   &       2   &   5     &     4   &  2.75   \\ \hline
  Pix2Pix    &      3   &       4   &   7     &     5   &  4.75   \\ \hline
\end{tabular}
\end{center}
    \caption{The total number of times each of the 12 textured models were identified as being textured using the data set by participants.}
\end{figure}


\subsubsection{Section 2}
\label{subsubsec:studysection2images}
\begin{figure}[H]
    \label{fig:studysection2images}
    \includegraphics[scale=0.25,center]{./Images/section2pt1.png}
    \includegraphics[scale=0.25,center]{./Images/section2pt2.png}
    \caption{A bar chart showing the percentage of textures that were identified as real.}
\end{figure}
In the second section, participants were shown one model of a bed that was textured using textures synthesised by both the Pix2Pix and BicycleGAN models. They were also shown the texture in 2 dimensions. Participants were then asked to classify each texture into one of the ten texture classes. The number of correct answers by class and AI model are displayed below:

\begin{center}
\begin{tabular}{ |c|c|c| } 
    \hline
    Texture Class & BicycleGAN & Pix2Pix \\ \hline
    Banded  	& 1   &   3\\  \hline
    Chequered	& 3   &   3\\  \hline
    Dotted	    & 2   &   6\\  \hline
    Foliage  	& 7   &   4\\  \hline
    Metal   	& 1   &   1\\  \hline
    Scaly  	    & 6   &   0\\  \hline
    Stone	    & 1   &   4\\  \hline
    Wood    	& 2   &   1\\  \hline
    Woven   	& 1   &   2\\  \hline
    Zigzagged	& 1   &   0\\  \hline \hline
    Average     & 2.5 & 2.4 \\  \hline
\end{tabular}
\end{center}




\section{Evaluation of User Study}
\label{sec:evaluation}
\subsection{Section 1 Evaluation}
\label{subsec:section1}

\begin{figure}[H]
    \label{fig:realvsbicycleganvspix2pixbarchartpercent}
    \includegraphics[scale=0.6,center]{./Images/Percentage_of_Textures_Identified_as_Real_Sorted_by_Model.png}
    \caption{A bar chart showing the percentage of textures that were identified as real.}
\end{figure}

The data set of real textures was only identified as being real 55\% of the time, this value can be used as a control to measure the two models' effectiveness at generating realistic outputs. This is only 5\% higher than in participants randomly selected between 'real' and 'fake'. The 3D models that were textured using the real datasets were still classified as being real 47.5\% and 27.5\% more often than the Pix2Pix and BicycleGAN models respectively. This implies that the Pix2Pix model generated more realistic textures than the BicycleGAN model.

Based on the written feedback at the end of the survey, many people found this part of the task challenging. This is likely due to how the real textures were applied to the models, how some classes looked similar to each other, and how many people did not have any prior experience with 3D models or Artificial Intelligence.

\subsection{Section 2 Evaluation}
\label{subsec:section2}
\begin{figure}[H]
    \label{fig:classcomparisonbarchart}
    \includegraphics[scale=0.6,center]{./Images/BicycleGAN_and_Pix2Pix_Percentage_of_Correctly_Identified_Textures_by_Texture_Type.png}
    \caption{A bar chart showing the percentage of correctly identified textures classes.}
\end{figure}

On average participants classified textures synthesised by the BicycleGAN model as the correct class 25\% of the time, while participants classed the textured generated by the Pix2Pix model as the correct class 24\% of the time. The two both performed better than the 10\% that would have been achieved by randomly selecting answers however, the results were still sub-par. The bar chart above shows how accurately participants classified each class. Overall the most easily distinguishable class was the 'Foliage' class generated by the BicycleGAN model and the worst performing was the 'Zigzagged' textures produced by the Pix2Pix model. The 'Scaly' class was often identified correctly by participants when the BicycleGAN model was used however, it was never guessed correctly when the Pix2Pix model was used. 

There are a number of reasons why this test received the results it did. The green colours (and other colours found in nature) normally associated with the 'Foliage' class are less present in the other real datasets. This makes it more distinguishable from the rest of the classes and the results could of varied if another class was introduced that people stereotypically think of as green. Additionally, participants frequently misidentified the 'Banded' class for wood which was most likely due to the generated example used being used was brown and had the appearance of wooden slacks, despite still technically being banded.

The Pix2Pix model did definitely struggle to produce identifiable textures for the 'Scaly' and 'Zigzagged' classes. Despite generating recognisable texture on the training set, when the model was applied to the test data set blurry and ambiguous images were produced. This is likely due to the complexity of the patterns compared to other some of the other patterns within the data set. Additionally, there was a lot of variety within the real images which may have made it more difficult for the GAN to find the overall pattern. This problem can be addressed by training the model for longer and adding additional examples to the training dataset. 


\section{Project Evaluation}
\label{sec:projectevaluation}
\subsection{Planning and Management}
\label{subsec:planningandmanagement}
Overall the project was managed well, however a large amount of progress was made early on in the project. Towards the end of the project progress definitely slowed down as many aspects were being fine tune. Perhaps some aspects of the projects could of been less perfected to instead introduce new features into the application.

Much of the GAN model training took place overnight while the PC would of not otherwise been used. This was a good way of still making progress on the project while not being available to work on the project as it have difficult to use the computer for other uses were training was occurring.

Some aspects of the report could of been worked on throughout the project instead of being worked on at the end, however there was some uncertainty as to what would be into the final version of the software.

\subsection{Project Limitations}
\label{subsec:projectlimitations}
Since there were many similarity between the wood textures and banded textures participants in the user survey often misidentified the banded textures with wooden ones and vice versa. This was likely due to the generated banded texture being coloured brown. Adding additional banded examples into the training dataset where that were not brown could overcome this limitation.

Another limitation is that harsh edges were produced where adjacent triangles in the model were not adjacent in the UV map. This created harsh seems along the models and may have lead to some confusion during the user survey.