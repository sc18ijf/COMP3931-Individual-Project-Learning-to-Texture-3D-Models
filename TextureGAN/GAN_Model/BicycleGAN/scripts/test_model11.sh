set -ex
# models
RESULTS_DIR='./results/model11'

# dataset
CLASS='model11_bicycle_gan'
DIRECTION='BtoA' # from domain A to domain B
LOAD_SIZE=512 # scale images to this size
CROP_SIZE=512 # then crop to this size
INPUT_NC=3  # number of channels in the input image
ASPECT_RATIO=1.0

# misc
GPU_ID=0   # gpu id
NUM_TEST=130 # number of input images duirng test
NUM_SAMPLES=12 # number of samples per input images


# command
CUDA_VISIBLE_DEVICES=${GPU_ID} python ./test.py \
  --dataroot ./datasets/model11 \
  --results_dir ${RESULTS_DIR} \
  --checkpoints_dir ./checkpoints/model11/ \
  --name ${CLASS} \
  --direction ${DIRECTION} \
  --load_size ${LOAD_SIZE} \
  --crop_size ${CROP_SIZE} \
  --input_nc ${INPUT_NC} \
  --num_test ${NUM_TEST} \
  --n_samples ${NUM_SAMPLES} \
  --aspect_ratio ${ASPECT_RATIO} \
  --center_crop \
  --no_flip \
  --no_encode
  #--preprocess scale_width
