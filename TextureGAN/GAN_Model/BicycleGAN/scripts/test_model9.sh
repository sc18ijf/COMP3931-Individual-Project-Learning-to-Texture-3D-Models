set -ex
# models
RESULTS_DIR='./results/model9'

# dataset
CLASS='model9_bicycle_gan'
DIRECTION='BtoA' # from domain A to domain B
LOAD_SIZE=512 # scale images to this size
CROP_SIZE=512 # then crop to this size
INPUT_NC=3  # number of channels in the input image

# misc
GPU_ID=0   # gpu id
NUM_TEST=130 # number of input images duirng test
NUM_SAMPLES=12 # number of samples per input images


# command
CUDA_VISIBLE_DEVICES=${GPU_ID} python ./test.py \
  --dataroot ./datasets/model9 \
  --results_dir ${RESULTS_DIR} \
  --checkpoints_dir ./checkpoints/model9/ \
  --name ${CLASS} \
  --direction ${DIRECTION} \
  --load_size ${LOAD_SIZE} \
  --input_nc ${INPUT_NC} \
  --num_test ${NUM_TEST} \
  --n_samples ${NUM_SAMPLES} \
  --no_flip \
  --crop_size ${CROP_SIZE} \
  --center_crop \
  --no_encode
#  --preprocess none \

