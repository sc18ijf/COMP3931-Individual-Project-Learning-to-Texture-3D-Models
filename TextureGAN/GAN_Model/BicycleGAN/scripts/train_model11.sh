set -ex
MODEL='bicycle_gan'
# dataset details
CLASS='model11'  # facades, day2night, edges2shoes, edges2handbags, maps
NZ=8
NO_FLIP=''
DIRECTION='BtoA'
LOAD_SIZE=512
CROP_SIZE=512
INPUT_NC=3
NITER=400
NITER_DECAY=400
SAVE_EPOCH=25
EPOCH_COUNT=2

# training
GPU_ID=0
DISPLAY_ID=$((GPU_ID*10+1))
CHECKPOINTS_DIR=./checkpoints/${CLASS}/
NAME=${CLASS}_${MODEL}

# command
CUDA_VISIBLE_DEVICES=${GPU_ID} python ./train.py \
  --display_id ${DISPLAY_ID} \
  --dataroot ./datasets/${CLASS} \
  --name ${NAME} \
  --model ${MODEL} \
  --direction ${DIRECTION} \
  --checkpoints_dir ${CHECKPOINTS_DIR} \
  --load_size ${LOAD_SIZE} \
  --nz ${NZ} \
  --input_nc ${INPUT_NC} \
  --niter ${NITER} \
  --niter_decay ${NITER_DECAY} \
  --use_dropout \
  --save_epoch_freq ${SAVE_EPOCH} \
 #--preprocess none  
# --crop_size ${CROP_SIZE} \

# --continue_train \
#--epoch_count ${EPOCH_COUNT} \

  
