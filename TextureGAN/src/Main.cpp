#include <QApplication>
#include <QVBoxLayout>
#include "Window.h"

int main(int argc, char *argv[]) { // main()
    // create the application
    QApplication app(argc, argv);

    // create a window widget
    Window *window = new Window(NULL);

    // resize the window
    window->resize(512, 612);

    // show the widget
    window->show();

    // start it running
    app.exec();

    // clean up
    delete window;

    // return to caller
    return 0;
} // main()
