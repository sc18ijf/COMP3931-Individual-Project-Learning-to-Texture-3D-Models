#ifndef __GL_MODEL_WIDGET_H__
#define __GL_MODEL_WIDGET_H__ 1

#include <cmath>
#include <GL/glu.h>
#include <QGLWidget>
#include <QGLWidget>
#include <QtGui>
#include <TextureCreator.h>
#include <TextureWidget.h>
#include <Python.h>
#include <stdio.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>


class ModelWidget : public QGLWidget {
    Q_OBJECT
public:
    ModelWidget(QWidget *parent);

    TextureWidget *textureWidget;


    QList <QString> modelsNames = {
            "Chair",
            "Cube",
            "Plant",
    };

    ////Model paths
    QList <QString> objStrings = {
            "../Models/Edited_Models/chair5.obj",
            "../Models/Edited_Models/cube.obj",
            "../Models/Edited_Models/plant.obj",
            "../Models/Edited_Models/vase.obj",
            "../Models/Edited_Models/plant2.obj",
            "../Models/Edited_Models/plant3.obj",
            "../Models/Edited_Models/bench.obj",
            "../Models/Edited_Models/wardrobe.obj",
            "../Models/Edited_Models/curtains.obj",
            "../Models/Edited_Models/guitar.obj",
            "../Models/Edited_Models/metalguitar.obj",
            "../Models/Edited_Models/tent.obj",
            "../Models/Edited_Models/bed.obj",
            "../Models/Edited_Models/plant4.obj",
            "../Models/Edited_Models/plant5.obj",
            "../Models/Edited_Models/airplane.obj",
            "../Models/Edited_Models/mantel.obj",
            "../Models/Edited_Models/bookcase.obj",
            "../Models/Edited_Models/plant6.obj",
            "../Models/Edited_Models/bed2.obj",
            "../Models/Edited_Models/guitar3.obj",
            "../Models/Edited_Models/sphere.obj",
            "../Models/Edited_Models/sphere2.obj",
    };
    static const int numberOfTextures = 4;

    int selectedModel = 0;

    int textureBlendPercentage = 0;
public
    Q_SLOTS:

            void changeObjectFile(int
    i);

    void renderNewModelSlot();

    void clearModel();

    void changeGPUValue(int i);

    void changeStyleTransferValue(int i);

    void changeStyleTransferImagePath(QString path);

    void changeStyleImpactValue(int i);

    void changeGANModel(int i);

    void changeTextureNumber(int i);
protected:


    ////Camera variables
    float cameraPosition[3] = {0, 0, 0};
    float cameraStartPosition[3] = {cameraPosition[0], cameraPosition[1], cameraPosition[2]};
    float cameraUp[3] = {0, 6, 0};
    float fov = 60;
    float radius = 0.1;
    float turningNumber = 0;
    float startingTurningNumber = turningNumber;
    float yaw = M_PI_2;
    float startingYaw = yaw;
    float camX = sin(turningNumber) * radius;
    float camZ = cos(turningNumber) * radius;
    float cameraDirection[3] = {cameraPosition[0] - 0, cameraPosition[1], cameraPosition[2] - radius};
    float cameraStartDirection[3] = {cameraDirection[0], cameraDirection[1], cameraDirection[2]};

    bool gpuValue = true;

    ////Data from OBJ Files
    std::array <std::vector<GLfloat>, numberOfTextures> objectVertices;
    std::array <std::vector<GLfloat>, numberOfTextures> objectUVs;
    std::array <std::vector<GLfloat>, numberOfTextures> objectNormals;
    std::array <std::vector<GLuint>, numberOfTextures> objectVertexIndices;
    std::array <std::vector<GLuint>, numberOfTextures> objectUVIndices;

    ////Functions
    void initializeGL();

    bool getOBJData(std::string fp,
                    std::vector <GLfloat> &out_vertices,
                    std::vector <GLfloat> &out_uvs,
                    std::vector <GLfloat> &out_normals,
                    std::vector <GLuint> &out_vertex_indices,
                    std::vector <GLuint> &out_uv_indices
    );

    void resizeGL(int w, int h);

    void paintGL();

    void drawObject();

    void keyPressEvent(QKeyEvent *key);

    void renderNewModel(string fp, array <vector<GLfloat>, numberOfTextures> &out_vertices,
                        array <vector<GLfloat>, numberOfTextures> &out_uvs,
                        array <vector<GLfloat>, numberOfTextures> &out_normals,
                        array <vector<GLuint>, numberOfTextures> &out_vertex_indices,
                        array <vector<GLuint>, numberOfTextures> &out_uv_indices);

private:

    void callPythonTestingScript();

    void callPythonStyleScript();

    string executeShell(const char *cmd);

    bool styleTransferCheck = false;

    string styleTransferImagePath = "";

    int selectedGANModel = 0;

    int textureNumber = 1;

    int selectedTexture = 0;

};

#endif
