#ifndef __GL_TEXTURE_WIDGET_H__
#define __GL_TEXTURE_WIDGET_H__ 1



#include <GL/glu.h>
#include <QGLWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsEllipseItem>
#include <algorithm>
#include <stdlib.h>
#include <TextureCreator.h>
#include <ctime>
#include <chrono>
#include <qt5/QtWidgets/QLabel>
#include <QMouseEvent>
#include <QGLWidget>
#include <QtGui>
#include <iostream>
#include <version>

#ifdef __cpp_lib_filesystem
#include <filesystem>
    namespace fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
#include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
//no filesystem support
#endif

using namespace std::chrono;

class TextureWidget : public QGLWidget { //
Q_OBJECT
public:
    TextureWidget(QWidget *parent);

    TextureCreator *textureCreator;

    int selectedUV = 1;

    int semanticColour = 0;
    int brushRadius = 1;
    int padding = 0;
    int brushPosition[2] = {0, 0};

    QPixmap UVMap;
    bool redrawUVMap = true;


    bool triangleAddReady = true;

    int numberOfFacades = 20;
    static const int facadeRes = 512;
    QColor textureWidgetColorArray[facadeRes][facadeRes];
    QColor textureWidgetColorArray2[facadeRes][facadeRes];
    const string pathString = "../eval/testset";
    const string testLocation = "add5Examples";
    const string pathTestString = "../GAN_Model/fullModel/datasets/add5Examples/test/facade_0.jpg";
    const string pathTestStringBicycleGAN = "../GAN_Model/fullModel/datasets/add5Examples/val/facade_0.jpg";
//    /home/izzy/Documents/COMP3931-Individual-Project-Learning-to-Texture-3D-Models/TextureGAN/GAN_Model/fullModel/datasets/addExamples/test

    static const int numberOfTextures = 10;
    QString semanticColourStrings[numberOfTextures] = {
            "blue",
            "red",
            "green",
            "cyan",
            "magenta",
            "yellow",
            "antiquewhite",
            "orange",
            "brown",
            "darkcyan",
    };

    QGraphicsScene *scene = new QGraphicsScene(QRect(0,0,width,height));
    QGraphicsView *view = new QGraphicsView(this);

    QPen qPen;
    QBrush qBrush;
    std::vector<QGraphicsPolygonItem*> pTriangleItems;
    std::vector<QGraphicsLineItem*> pTriangleLines;
    QGraphicsEllipseItem* pCircleBrush = new QGraphicsEllipseItem(0,0,brushRadius*2,brushRadius*2);

    uint64_t t1 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    uint64_t t2 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

    //TODO Triangle struct
    std::vector<std::array<int, 6>> triangles;
    std::vector<std::array<int, 2>> filledTriangles;
    int filledTrianglesSize = 0;
    int filledTrianglesSizeTemp = 0;

    std::vector<GLfloat> objectUVs;
    std::vector<GLuint> objectUVIndices;

    void setUVData(
            std::vector<GLfloat> &objectUVs,
            std::vector<GLuint> &objectUVIndices
    );

    bool mouseDown = false;

    int startNum = 0;

    void generateTestFacade();

public
    Q_SLOTS:

    void clearFilledTriangles();

    void changeUVMap(int i);

    void changeSemanticColour(int i);

    void showUVMap(int i);

    void generateFacades();

    void changeBrushSize(int brushSize);

    void changeStartingNumber(QString startingNumber);

    void setFixedSizeSlot();

    void setUnfixedSizeSlot();

    void changePaddingNumber(QString startingNumber);

protected:
    void initializeGL();

    void resizeGL(int w, int h);

    int width = 100;
    int height = width;

    bool isInside(float x1, float y1, float x2, float y2, float x3, float y3, float uLocation, float vLocation,
                  float radius);

private:
    void paintEvent(QPaintEvent *event);

    void makeTriangles();

    void makeTriangles2();

    void drawNewFilledTriangles(std::vector<std::array<int, 2>> triangleData);

    void addTriangles(QMouseEvent *event);

    bool showUVOption = true;

    bool showUVMapBackground = true;

    bool colourMatch(QColor pixelColor);

    int tempGlobalWidth = width;
    int tempGlobalHeight = height;

    QColor colourMatchReturnColour(QColor pixelColor);

    array<int, TextureWidget::numberOfTextures> getRandomIntArray(int i);

    int colourMatchReturnColourInt(QColor pixelColor);

    bool circleLineSegCollision(float x1, float y1, float x2, float y2, float x, float y, float radius);


    int sign(int x);

    void drawEmptyTriangles();

    void mouseEvent(QMouseEvent *event);

    void makeScene();

    bool eventFilter(QObject *obj, QEvent *event);

    void redrawFilledTrianglesWithEdges(vector<array<int, 2>> triangleData);

    void remakeScene();

    void remakeSceneNoUVMapOrCircle();

    void redrawFilledTrianglesNoEdges(vector<array<int, 2>> triangleData);
};

#endif
