#ifndef __GL_WINDOW_H__
#define __GL_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QBoxLayout>
#include <QGridLayout>
#include <QTimer>
#include <QCheckBox>
#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <qsizepolicy.h>
#include <QIntValidator>
#include "TextureWidget.h"
#include "ModelWidget.h"

class Window : public QWidget {
    Q_OBJECT
public:
    Window(QWidget *parent);

    ~Window();

    QMenuBar *menuBar;
    QMenu *fileMenu;
    QAction *quitAction;
    QAction *userAction;
    QAction *devAction;

    // window layout
    QBoxLayout *layoutLayout;
    QBoxLayout *windowLayout;
    QBoxLayout *topButtonLayout;
    QBoxLayout *bottomButtonLayout;
    QBoxLayout *widgetLayout;
    QBoxLayout *checkboxLayout;
    QBoxLayout *paddingLayout;
    QBoxLayout *ganModelLayout;
    QBoxLayout *bicycleGANTextureLayout;
    QGridLayout *buttonsLayout;

    QComboBox *modelSelectionCbo;
    QComboBox *ganModelSelectionCbo;
    QPushButton *renderBtn;
    QIntValidator *intValid = new QIntValidator(0, INT_MAX, this);
    QIntValidator *int255Valid = new QIntValidator(0, 255, this);
    QLineEdit *facadeNumberTxt;
    QLineEdit *paddingTxt;
    QPushButton *facadeBtn;
    QPushButton *setFixedSizeBtn;
    QPushButton *setUnfixedSizeBtn;
    QComboBox *textureSelectionCbo;
    QPushButton *clearBtn;
    QSlider *brushSlider;
    QSlider *bicycleGANTextureSlider;
    QCheckBox *gpuAcceleratedCheck;
    QLabel *bicycleGANTextureSliderLabel;

    QCheckBox *showUV;

    QCheckBox *styleTransferCheck;
    QLineEdit *styleTransferText;

    // beneath that, the main widget
    TextureWidget *textureWidget;
    ModelWidget *modelWidget;

    void resizeGL(int w, int h);

    ////List of Models
    QList<QString> modelsNames = {
            "Chair",
            "Cube",
            "Plant",
            "Vase",
            "Tree",
            "Plant 2",
            "Bench",
            "Wardrobe",
            "Curtains",
            "Guitar",
            "Metalguitar",
            "Tent",
            "Bed",
            "Plant4",
            "Plant 5",
            "Airplane",
            "Mantel",
            "Bookcase",
            "Plant 6",
            "Bed 2",
            "Metal guitar 2",
            "Sphere 1",
            "Sphere 2",
    };

    QList<QString> ganModelsNames = {
            "BicycleGAN",
            "Pix2Pix",
    };

    QList<QString> textureTypes = {
            "Wood - blue",
            "Metal - red",
            "Foliage - green",
            "Stone - cyan",
            "Banded - magenta",
            "Chequered - yellow",
            "Dotted - antiquewhite",
            "Scaly - orange",
            "Woven - brown",
            "Zigzagged - darkcyan",
    };

public Q_SLOTS:

            void closeWindow();

    void showUserUI();

    void showDevUI();

    void hideTextureSlider(int i);
};

#endif
