/****************************************************************************
** Meta object code from reading C++ file 'TextureWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "TextureWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'TextureWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TextureWidget_t {
    QByteArrayData data[15];
    char stringdata0[215];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TextureWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TextureWidget_t qt_meta_stringdata_TextureWidget = {
    {
QT_MOC_LITERAL(0, 0, 13), // "TextureWidget"
QT_MOC_LITERAL(1, 14, 20), // "clearFilledTriangles"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 11), // "changeUVMap"
QT_MOC_LITERAL(4, 48, 1), // "i"
QT_MOC_LITERAL(5, 50, 20), // "changeSemanticColour"
QT_MOC_LITERAL(6, 71, 9), // "showUVMap"
QT_MOC_LITERAL(7, 81, 15), // "generateFacades"
QT_MOC_LITERAL(8, 97, 15), // "changeBrushSize"
QT_MOC_LITERAL(9, 113, 9), // "brushSize"
QT_MOC_LITERAL(10, 123, 20), // "changeStartingNumber"
QT_MOC_LITERAL(11, 144, 14), // "startingNumber"
QT_MOC_LITERAL(12, 159, 16), // "setFixedSizeSlot"
QT_MOC_LITERAL(13, 176, 18), // "setUnfixedSizeSlot"
QT_MOC_LITERAL(14, 195, 19) // "changePaddingNumber"

    },
    "TextureWidget\0clearFilledTriangles\0\0"
    "changeUVMap\0i\0changeSemanticColour\0"
    "showUVMap\0generateFacades\0changeBrushSize\0"
    "brushSize\0changeStartingNumber\0"
    "startingNumber\0setFixedSizeSlot\0"
    "setUnfixedSizeSlot\0changePaddingNumber"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TextureWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x0a /* Public */,
       3,    1,   65,    2, 0x0a /* Public */,
       5,    1,   68,    2, 0x0a /* Public */,
       6,    1,   71,    2, 0x0a /* Public */,
       7,    0,   74,    2, 0x0a /* Public */,
       8,    1,   75,    2, 0x0a /* Public */,
      10,    1,   78,    2, 0x0a /* Public */,
      12,    0,   81,    2, 0x0a /* Public */,
      13,    0,   82,    2, 0x0a /* Public */,
      14,    1,   83,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   11,

       0        // eod
};

void TextureWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TextureWidget *_t = static_cast<TextureWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clearFilledTriangles(); break;
        case 1: _t->changeUVMap((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->changeSemanticColour((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->showUVMap((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->generateFacades(); break;
        case 5: _t->changeBrushSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->changeStartingNumber((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->setFixedSizeSlot(); break;
        case 8: _t->setUnfixedSizeSlot(); break;
        case 9: _t->changePaddingNumber((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject TextureWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_TextureWidget.data,
      qt_meta_data_TextureWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TextureWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TextureWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TextureWidget.stringdata0))
        return static_cast<void*>(this);
    return QGLWidget::qt_metacast(_clname);
}

int TextureWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
