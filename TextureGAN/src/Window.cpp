#include "Window.h"

// constructor / destructor
Window::Window(QWidget *parent) : QWidget(parent) {
    ////Create menu bar
    menuBar = new QMenuBar(this);
    fileMenu = menuBar->addMenu("File");
    userAction = new QAction("&User Mode", this);
    devAction = new QAction("&Developer Mode", this);
    quitAction = new QAction("&Quit", this);
    fileMenu->addAction(userAction);
    fileMenu->addAction(devAction);
    fileMenu->addAction(quitAction);

    ///Create the window layouts
    layoutLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    windowLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    paddingLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    ganModelLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *sliderLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *bicycleGANTextureLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QHBoxLayout *styleSliderLayout = new QHBoxLayout();
    widgetLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    topButtonLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    bottomButtonLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    buttonsLayout = new QGridLayout();

    ///UI Widgets
    textureSelectionCbo = new QComboBox(this);
    textureSelectionCbo->addItems(textureTypes);
    clearBtn = new QPushButton(this);
    clearBtn->setText("Clear");

    modelSelectionCbo = new QComboBox(this);
    modelSelectionCbo->addItems(modelsNames);

    QLabel *ganModelLbl = new QLabel("GAN Model:");
    ganModelLbl->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    ganModelSelectionCbo = new QComboBox(this);
    ganModelSelectionCbo->addItems(ganModelsNames);
    ganModelSelectionCbo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    renderBtn = new QPushButton(this);
    renderBtn->setText("Render");

    QLabel *paddingLbl = new QLabel("Padding Size:");
    paddingTxt = new QLineEdit(this);
    paddingTxt->setText("0");
    paddingTxt->setValidator(int255Valid);


    facadeNumberTxt = new QLineEdit(this);
    facadeNumberTxt->setValidator(intValid);
    setFixedSizeBtn = new QPushButton(this);
    setUnfixedSizeBtn = new QPushButton(this);
    setFixedSizeBtn->setText("Set fixed Size");
    setUnfixedSizeBtn->setText("Set unfixed Size");


    facadeBtn = new QPushButton(this);
    facadeBtn->setText("Facade Creation");

    showUV = new QCheckBox("Show UV", this);
    showUV->setCheckState(Qt::Checked);

    styleTransferCheck = new QCheckBox("Style Transfer", this);
    QHBoxLayout *styleLayout = new QHBoxLayout();
    QLabel *styleLbl = new QLabel("Image Path:");
    styleTransferText = new QLineEdit(this);
    styleLayout->addWidget(styleLbl);
    styleLayout->addWidget(styleTransferText);

    QLabel *styleSliderLabel = new QLabel("Style Impact:");
    QSlider *styleSlider = new QSlider(Qt::Horizontal, this);
    styleSlider->setMinimum(0);
    styleSlider->setMaximum(100);

    bicycleGANTextureSliderLabel = new QLabel("BicycleGAN Texture:",this);
    bicycleGANTextureSlider = new QSlider(Qt::Horizontal, this);
    bicycleGANTextureSlider->setMinimum(0);
    bicycleGANTextureSlider->setMaximum(10);
    bicycleGANTextureLayout->addWidget(bicycleGANTextureSliderLabel);
    bicycleGANTextureLayout->addWidget(bicycleGANTextureSlider);


    gpuAcceleratedCheck = new QCheckBox("GPU Acceleration", this);
    gpuAcceleratedCheck->setCheckState(Qt::Checked);

    QLabel *brushLbl = new QLabel("Brush Size:");
    brushSlider = new QSlider(Qt::Horizontal, this);



    //Create main OpenGL widget
    modelWidget = new ModelWidget(this);
    textureWidget = modelWidget->textureWidget;
    modelWidget->setFocusPolicy(Qt::StrongFocus);
    textureWidget->setFocusPolicy(Qt::NoFocus);
    textureWidget->setMouseTracking(true);
    textureSelectionCbo->setFocusPolicy(Qt::NoFocus);
    paddingTxt->setFocusPolicy(Qt::ClickFocus);
    facadeNumberTxt->setFocusPolicy(Qt::ClickFocus);
    modelSelectionCbo->setFocusPolicy(Qt::NoFocus);
    ganModelSelectionCbo->setFocusPolicy(Qt::NoFocus);
    renderBtn->setFocusPolicy(Qt::NoFocus);
    facadeBtn->setFocusPolicy(Qt::NoFocus);
    showUV->setFocusPolicy(Qt::NoFocus);
    clearBtn->setFocusPolicy(Qt::NoFocus);

    //8 factor multisampling (antialias)
    QGLFormat format;
    format.setSamples(8);
    textureWidget->setFormat(format);
    modelWidget->setFormat(format);

    ///Add widgets to layout
    layoutLayout->setMenuBar(menuBar);
    layoutLayout->addLayout(windowLayout);
    layoutLayout->addLayout(widgetLayout);
    widgetLayout->addLayout(buttonsLayout);

    sliderLayout->addWidget(brushLbl);
    sliderLayout->addWidget(brushSlider);

    styleSliderLayout->addWidget(styleSliderLabel);
    styleSliderLayout->addWidget(styleSlider);

    paddingLayout->addWidget(paddingLbl);
    paddingLayout->addWidget(paddingTxt);

    ganModelLayout->addWidget(ganModelLbl);
    ganModelLayout->addWidget(ganModelSelectionCbo);

    windowLayout->addWidget(textureWidget);
    windowLayout->addWidget(modelWidget);


    ////Column 1
    buttonsLayout->addLayout(sliderLayout, 0, 0);
    buttonsLayout->addLayout(ganModelLayout, 1, 0);
    buttonsLayout->addWidget(textureSelectionCbo, 2, 0);
    buttonsLayout->addWidget(modelSelectionCbo, 3, 0);
    buttonsLayout->addWidget(styleTransferCheck, 4, 0);
    buttonsLayout->addLayout(styleSliderLayout, 5, 0);
    ////Column 2
    buttonsLayout->addLayout(paddingLayout, 0, 1);
    buttonsLayout->addLayout(bicycleGANTextureLayout, 1, 1);
    buttonsLayout->addWidget(clearBtn, 2, 1);
    buttonsLayout->addWidget(gpuAcceleratedCheck, 3, 1);
    buttonsLayout->addWidget(renderBtn, 4, 1);
    buttonsLayout->addLayout(styleLayout, 5, 1);
    ////Column 3
    buttonsLayout->addWidget(showUV, 1, 2);
    buttonsLayout->addWidget(facadeNumberTxt, 2, 2);
    buttonsLayout->addWidget(setUnfixedSizeBtn, 3, 2);
    buttonsLayout->addWidget(facadeBtn, 4, 2);
    buttonsLayout->addWidget(setFixedSizeBtn, 3, 3);

    ///Connections
    QObject::connect(modelSelectionCbo, SIGNAL(currentIndexChanged(int)), modelWidget, SLOT(changeObjectFile(int)));
    QObject::connect(ganModelSelectionCbo, SIGNAL(currentIndexChanged(int)), modelWidget, SLOT(changeGANModel(int)));
    QObject::connect(ganModelSelectionCbo, SIGNAL(currentIndexChanged(int)), this, SLOT(hideTextureSlider(int)));
    QObject::connect(bicycleGANTextureSlider, SIGNAL(valueChanged(int)), modelWidget, SLOT(changeTextureNumber(int)));
    QObject::connect(modelSelectionCbo, SIGNAL(currentIndexChanged(int)), textureWidget, SLOT(changeUVMap(int)));
    QObject::connect(textureSelectionCbo, SIGNAL(currentIndexChanged(int)), textureWidget,
                     SLOT(changeSemanticColour(int)));
    QObject::connect(clearBtn, SIGNAL(clicked(bool)), textureWidget, SLOT(clearFilledTriangles()));
    QObject::connect(clearBtn, SIGNAL(clicked(bool)), modelWidget, SLOT(clearModel()));
    QObject::connect(renderBtn, SIGNAL(clicked(bool)), modelWidget, SLOT(renderNewModelSlot()));
    QObject::connect(facadeNumberTxt, SIGNAL(textChanged(const QString &)), textureWidget,
                     SLOT(changeStartingNumber(QString)));
    QObject::connect(paddingTxt, SIGNAL(textChanged(const QString &)), textureWidget,
                     SLOT(changePaddingNumber(QString)));
    QObject::connect(setFixedSizeBtn, SIGNAL(clicked(bool)), textureWidget, SLOT(setFixedSizeSlot()));
    QObject::connect(setUnfixedSizeBtn, SIGNAL(clicked(bool)), textureWidget, SLOT(setUnfixedSizeSlot()));
    QObject::connect(facadeBtn, SIGNAL(clicked(bool)), textureWidget, SLOT(generateFacades()));
    QObject::connect(showUV, SIGNAL(stateChanged(int)), textureWidget, SLOT(showUVMap(int)));
    QObject::connect(gpuAcceleratedCheck, SIGNAL(stateChanged(int)), modelWidget, SLOT(changeGPUValue(int)));
    QObject::connect(styleTransferCheck, SIGNAL(stateChanged(int)), modelWidget, SLOT(changeStyleTransferValue(int)));
    QObject::connect(styleTransferText, SIGNAL(textChanged(const QString &)), modelWidget,
                     SLOT(changeStyleTransferImagePath(const QString &)));
    QObject::connect(styleSlider, SIGNAL(valueChanged(int)), modelWidget, SLOT(changeStyleImpactValue(int)));
    QObject::connect(quitAction, SIGNAL(triggered()), this, SLOT(closeWindow()));
    QObject::connect(userAction, SIGNAL(triggered()), this, SLOT(showUserUI()));
    QObject::connect(devAction, SIGNAL(triggered()), this, SLOT(showDevUI()));
    QObject::connect(brushSlider, SIGNAL(valueChanged(int)), textureWidget, SLOT(changeBrushSize(int)));

    this->showUserUI();

    QTimer *frameTimer = new QTimer(this);
    QObject::connect(frameTimer, SIGNAL(timeout()), modelWidget, SLOT(updateGL()));
    float frameRate = 1000 / 60;
    frameTimer->setInterval(frameRate);
    frameTimer->start();

    this->resize(700, 300);
}

void Window::resizeGL(int w, int h) {
    int textureWidgetWidth = w / 2;
    textureWidget->resize(textureWidgetWidth, textureWidgetWidth);
}

Window::~Window() { // destructor
    delete windowLayout;
    delete quitAction;
    delete fileMenu;
    delete menuBar;
    //TODO remove additional widgets
}

void Window::closeWindow() {
    this->close();
}

void Window::showUserUI() {
    facadeNumberTxt->hide();
    setUnfixedSizeBtn->hide();
    setFixedSizeBtn->hide();
    facadeBtn->hide();
}

void Window::showDevUI() {
    facadeNumberTxt->show();
    setUnfixedSizeBtn->show();
    setFixedSizeBtn->show();
    facadeBtn->show();
}

void Window::hideTextureSlider(int i){
    if (i == 0){
        bicycleGANTextureSlider->show();
        bicycleGANTextureSliderLabel->show();
        modelWidget->changeTextureNumber(0);
        bicycleGANTextureSlider->setValue(0);
    }else if(i == 1){
        bicycleGANTextureSlider->hide();
        bicycleGANTextureSliderLabel->hide();
        modelWidget->changeTextureNumber(-1);
    }
}
