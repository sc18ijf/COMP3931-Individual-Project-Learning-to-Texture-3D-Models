#include "TextureCreator.h"

TextureCreator::TextureCreator() {
    stringAppender();
    imageLoader(textureStrings, textures);
}

void TextureCreator::stringAppender() {
    int types = 10;
    QString dir;

    for (int i = 0; i < types; i++) {
        if (i <= 3) {
            dir = "../Textures/fmd/image/";
            for (int j = 1; j <= 100; j++) {
                QString number = QString::number(j);
                textureStrings.append(dir + folderTextureNames[i] + "/" + folderTextureNames[i] + "_" + number + ".jpg");
            }
        } else {
            dir = "../Textures/dtd/images/";
            for (int j = 1; j <= 120; j++) {
                QString number = QString::number(j);
                textureStrings.append(dir + folderTextureNames[i] + "/" + folderTextureNames[i] + "_" + number + ".jpg");
            }
        }

        /// Bash command to rename files in a dir
        //  ls | cat -n | while read n f; do   mv "$f" "${PWD##*/}_$n.jpg"; done
    }
}


void TextureCreator::imageLoader(QStringList sImage, GLuint texture[]) {
    numberOfTextures = 4 + 10; //

    //arrays to store channel and image sizes
    int nChannels[numberOfTextures];
    unsigned int imageWidth[constNumberOfTextures];
    unsigned int imageHeight[constNumberOfTextures];

    //Generate textures in memory
    glGenTextures(numberOfTextures, texture);

    QList<QImage> pQImages;

    //For each image
    for (int i = 0; i < numberOfTextures; i++) {
        //Convert to QImage
        pQImages.push_back(QImage(sImage[i]));
        //and get size
        imageWidth[i] = pQImages[i].width();
        imageHeight[i] = pQImages[i].height();

        //set number of channels
        nChannels[i] = 3;
        if (sImage[i].endsWith("png")) {
            nChannels[i] = 4;
        }

        //Set texture parameters
        glBindTexture(GL_TEXTURE_2D, texture[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        //set format based on number of channels
        GLint format;
        if (nChannels[i] == 3) {
            pQImages[i] = pQImages[i].mirrored(false, false).convertToFormat(QImage::Format_RGB888);
            format = GL_RGB;
        } else {
            pQImages[i] = pQImages[i].mirrored(false, false).convertToFormat(QImage::Format_RGBA8888);
            format = GL_RGBA;
        }

        //Set texture
        glTexImage2D(GL_TEXTURE_2D,
                     0, format, imageWidth[i], imageHeight[i], 0, format, GL_UNSIGNED_BYTE,
                     pQImages[i].constBits());
    }
}
