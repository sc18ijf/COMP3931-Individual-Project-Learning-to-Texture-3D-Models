/****************************************************************************
** Meta object code from reading C++ file 'ModelWidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "ModelWidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ModelWidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ModelWidget_t {
    QByteArrayData data[13];
    char stringdata0[194];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ModelWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ModelWidget_t qt_meta_stringdata_ModelWidget = {
    {
QT_MOC_LITERAL(0, 0, 11), // "ModelWidget"
QT_MOC_LITERAL(1, 12, 16), // "changeObjectFile"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 1), // "i"
QT_MOC_LITERAL(4, 32, 18), // "renderNewModelSlot"
QT_MOC_LITERAL(5, 51, 10), // "clearModel"
QT_MOC_LITERAL(6, 62, 14), // "changeGPUValue"
QT_MOC_LITERAL(7, 77, 24), // "changeStyleTransferValue"
QT_MOC_LITERAL(8, 102, 28), // "changeStyleTransferImagePath"
QT_MOC_LITERAL(9, 131, 4), // "path"
QT_MOC_LITERAL(10, 136, 22), // "changeStyleImpactValue"
QT_MOC_LITERAL(11, 159, 14), // "changeGANModel"
QT_MOC_LITERAL(12, 174, 19) // "changeTextureNumber"

    },
    "ModelWidget\0changeObjectFile\0\0i\0"
    "renderNewModelSlot\0clearModel\0"
    "changeGPUValue\0changeStyleTransferValue\0"
    "changeStyleTransferImagePath\0path\0"
    "changeStyleImpactValue\0changeGANModel\0"
    "changeTextureNumber"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ModelWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x0a /* Public */,
       4,    0,   62,    2, 0x0a /* Public */,
       5,    0,   63,    2, 0x0a /* Public */,
       6,    1,   64,    2, 0x0a /* Public */,
       7,    1,   67,    2, 0x0a /* Public */,
       8,    1,   70,    2, 0x0a /* Public */,
      10,    1,   73,    2, 0x0a /* Public */,
      11,    1,   76,    2, 0x0a /* Public */,
      12,    1,   79,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

       0        // eod
};

void ModelWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ModelWidget *_t = static_cast<ModelWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->changeObjectFile((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->renderNewModelSlot(); break;
        case 2: _t->clearModel(); break;
        case 3: _t->changeGPUValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->changeStyleTransferValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->changeStyleTransferImagePath((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->changeStyleImpactValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->changeGANModel((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->changeTextureNumber((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject ModelWidget::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_ModelWidget.data,
      qt_meta_data_ModelWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ModelWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ModelWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ModelWidget.stringdata0))
        return static_cast<void*>(this);
    return QGLWidget::qt_metacast(_clname);
}

int ModelWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
