#include "ModelWidget.h"

ModelWidget::ModelWidget(QWidget *parent) : QGLWidget(parent) { // constructor
//  create a texture widget that is linked to the model widget
    textureWidget = new TextureWidget(parent);
}

void ModelWidget::initializeGL() {
//    Loads first model and setting up OpenGL lighting
    glClearColor(0.1, 0.1, 0.1, 0.1);
    getOBJData(objStrings[0].toStdString(),
               objectVertices[0],
               objectUVs[0],
               objectNormals[0],
               objectVertexIndices[0],
               objectUVIndices[0]);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_TEXTURE_2D);
    textureWidget->textureCreator->imageLoader(textureWidget->textureCreator->textureStrings,
                                               textureWidget->textureCreator->textures);

    ///TODO Change lighting
    glEnable(GL_COLOR_MATERIAL);
    GLfloat light_pos[] = {0., 10., 0., 0.};
    GLfloat spot_direction[] = {0, -1, 0, 0};
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 20);

    GLfloat light1Ambient[] = {0.3, 0.3, 0.3, 0.0};
    GLfloat light1Diffuse[] = {2.0, 2.0, 2.0, 0.0};
    GLfloat light1Specular[] = {0.2, 0.2, 0.2, 0.0};
    GLfloat light1Position[4] = {0, 10, -0, 1};
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1Ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1Diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1Specular);
    glLightfv(GL_LIGHT1, GL_POSITION, light1Position);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void ModelWidget::resizeGL(int w, int h) { // resizeGL()
    // set the viewport to the entire widget
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float width = w;
    float height = h;
    gluPerspective(100, width / height, 0.01, sqrt(pow(1000, 2) + pow(1000, 2)) + pow(1000, 2));
    glMatrixMode(GL_MODELVIEW);
}

void ModelWidget::changeObjectFile(int i) {
//    slot for changing 3d model
    selectedModel = i;
    clearModel();
}

void ModelWidget::changeGANModel(int i) {
//    slot for changing GAN model
    selectedGANModel = i;
}

void ModelWidget::changeGPUValue(int i) {
//    slot for changing GPU/CPU computation
    if (i == 0) {
        gpuValue = false;
    } else {
        gpuValue = true;
    }
}

void ModelWidget::changeStyleImpactValue(int i) {
    textureBlendPercentage = i;
}

void ModelWidget::changeStyleTransferValue(int i) {
    if (i == 0) {
        styleTransferCheck = false;
    } else {
        styleTransferCheck = true;
    }
}

void ModelWidget::changeStyleTransferImagePath(QString path) {
    styleTransferImagePath = path.toUtf8().constData();
}


std::string ModelWidget::executeShell(const char *cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

void ModelWidget::callPythonStyleScript() {
    //TODO 5 is static blur amount
    std::string selectedTexture = textureWidget->textureCreator->textureStrings[textureNumber].toUtf8().constData();

    std::string cmd =
            "python ../Neural_Style_Transfer/styleTransfer.py " + selectedTexture + " " +
            styleTransferImagePath + " " + "0";
    const char *fullCmd = cmd.c_str();
    executeShell(fullCmd);
}

void ModelWidget::callPythonTestingScript() {
//    Calls the relevant testing script based off of settings
    std::string testModel;
    std::string dataRoot;
    std::string direction;
    std::string model;
    std::string name;


    //IF GPU
    if (gpuValue) {
        if (selectedGANModel == 0) {
            executeShell(
                    "python ../GAN_Model/BicycleGAN/test.py --dataroot ../GAN_Model/fullModel/datasets/add5Examples --checkpoints_dir ../GAN_Model/BicycleGAN/checkpoints/model9/ --direction BtoA --name model9_bicycle_gan --n_samples 12 --num_test 1 --load_size 512 --crop_size 512 --input_nc 3 --no_flip --center_crop --no_encode");
        } else if (selectedGANModel == 1) {
            executeShell(
                    "python ../GAN_Model/fullModel/test.py --dataroot ../GAN_Model/fullModel/datasets/add5Examples --checkpoints_dir ../GAN_Model/fullModel/checkpoints/ --direction BtoA --model pix2pix --name model8 --num_test 1 --preprocess scale_width --load_size 512");
        }
    } else {
        if (selectedGANModel == 0) {
            executeShell(
                    "python ../GAN_Model/BicycleGAN/test.py --dataroot ../GAN_Model/fullModel/datasets/add5Examples --checkpoints_dir ../GAN_Model/BicycleGAN/checkpoints/model9/ --direction BtoA --name model9_bicycle_gan --n_samples 12 --num_test 1 --load_size 512 --crop_size 512 --input_nc 3 --no_flip --center_crop --no_encode --gpu_ids -1");
        } else if (selectedGANModel == 1) {
            executeShell(
                    "python ../GAN_Model/fullModel/test.py --dataroot ../GAN_Model/fullModel/datasets/add5Examples --checkpoints_dir ../GAN_Model/fullModel/checkpoints/ --direction BtoA --model pix2pix --name model8 --num_test 1 --preprocess scale_width --load_size 512 --gpu_ids -1 ");
        }

    }
    qDebug() << "Test Complete";
}

void ModelWidget::renderNewModelSlot() {
    qDebug() << "generating";
    this->textureWidget->generateTestFacade();
    qDebug() << "testing";
    callPythonTestingScript();

    if (styleTransferCheck && styleTransferImagePath != "")
        callPythonStyleScript();

    if (styleTransferCheck ){// && //textureBlendPercentage != 0) {
        QImage ganImage = QImage(textureWidget->textureCreator->textureStrings[textureNumber]);
        QImage styleImage = QImage("./results/facades_Add5_10/test_latest/images/style.png");

        ganImage = ganImage.convertToFormat(QImage::Format_ARGB32);
        styleImage = styleImage.convertToFormat(QImage::Format_ARGB32);
        float transparency = (float) textureBlendPercentage / 100;
        QImage mixedImage = QImage(ganImage.size(), QImage::Format_ARGB32);
        for (int x = 0; x < ganImage.width(); x++) {
            for (int y = 0; y < styleImage.height(); y++) {
                QColor ganColor = ganImage.pixelColor(x, y);
                QColor styleColor = styleImage.pixelColor(x, y);

                QColor mixedColor = QColor(
                        ganColor.red() * (1.0 - transparency) + styleColor.red() * transparency,
                        ganColor.green() * (1.0 - transparency) + styleColor.green() * transparency,
                        ganColor.blue() * (1.0 - transparency) + styleColor.blue() * transparency,
                        255);

                mixedImage.setPixelColor(x, y, mixedColor);
            }
        }

        mixedImage.save("./results/facades_Add5_10/test_latest/images/style_GAN_mix.png");
    }

    //And reload opengl textures
    textureWidget->textureCreator->imageLoader(textureWidget->textureCreator->textureStrings,
                                               textureWidget->textureCreator->textures);

    renderNewModel(objStrings[selectedModel].toStdString(), objectVertices, objectUVs, objectNormals,
                   objectVertexIndices,
                   objectUVIndices);
}

void ModelWidget::renderNewModel(std::string fp, std::array<std::vector<float>, numberOfTextures> &out_vertices,
                                 std::array<std::vector<float>, numberOfTextures> &out_uvs,
                                 std::array<std::vector<float>, numberOfTextures> &out_normals,
                                 std::array<std::vector<GLuint>, numberOfTextures> &out_vertex_indices,
                                 std::array<std::vector<GLuint>, numberOfTextures> &out_uv_indices) {

    //Remove whatever is in the buffer
    for (int i = 0; i < numberOfTextures; i++) {
        out_vertices[i].clear();
        out_uvs[i].clear();
        out_normals[i].clear();
        out_vertex_indices[i].clear();
        out_uv_indices[i].clear();
    }

    //Face-based indices
    std::vector<GLuint> vertexIndices, uvIndices, normalIndices, faces;

    //Temporary data
    std::vector<std::array<float, 3>> tempVertices;
    std::vector<std::array<float, 2>> tempUvs;
    std::vector<std::array<float, 3>> tempNormals;

    const char *path = fp.c_str();
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        qDebug() << "Cannot open object file !";
        return;
    }

    int k = 0;
    ////Loop over all of file until EOF to get of OBJ data
    while (1) {
        char lineHeader[128];
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break;

        ////If vertex data is found
        if (strcmp(lineHeader, "v") == 0) {
            float vertexX;
            float vertexY;
            float vertexZ;
            ///Get vertices and add to temporary vertex array
            fscanf(file, "%f %f %f\n", &vertexX, &vertexY, &vertexZ);
            std::array<float, 3> vertices = {vertexX, vertexY, vertexZ};
            tempVertices.push_back(vertices);
        }
            ////If vertex texture data is found add UV coordinates to temporary UV array
        else if (strcmp(lineHeader, "vt") == 0) {
            float uvU;
            float uvV;
            fscanf(file, "%f %f\n", &uvU, &uvV);
            std::array<float, 2> uv = {uvU, 1.0f - uvV};
            tempUvs.push_back(uv);
        }
            ////If vertex normal data is found add normal data to temporary normal array
        else if (strcmp(lineHeader, "vn") == 0) {
            float normalX;
            float normalY;
            float normalZ;
            fscanf(file, "%f %f %f\n", &normalX, &normalY, &normalZ);
            std::array<float, 3> normals = {normalX, normalY, normalZ};
            tempNormals.push_back(normals);
        }
            ////If face data is found add the 3 vertex indices, 3 UV indices and 3 normal indices to correct arrays.
            ////A face is always made up of triangles as as OBJ file has been triangulated beforehand
        else if (strcmp(lineHeader, "f") == 0) {
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0],
                                 &normalIndex[0],
                                 &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2],
                                 &normalIndex[2]);

            if (matches != 9) {
                qDebug() << "Model is not triangulated";
                return;
            }

            // 1 is subtracted to facilitate 0 index
            faces.push_back(k);
            vertexIndices.push_back(vertexIndex[0] - 1);
            vertexIndices.push_back(vertexIndex[1] - 1);
            vertexIndices.push_back(vertexIndex[2] - 1);
            uvIndices.push_back(uvIndex[0] - 1);
            uvIndices.push_back(uvIndex[1] - 1);
            uvIndices.push_back(uvIndex[2] - 1);
            normalIndices.push_back(normalIndex[0] - 1);
            normalIndices.push_back(normalIndex[1] - 1);
            normalIndices.push_back(normalIndex[2] - 1);
            k++;
        }
    }


    ////Once all vertex, UV, normal and face data has been obtain sort each data in order stated by face indices
    for (int i = 0; i < vertexIndices.size(); i++) {
        bool flag = false;
        float vertex2x, vertex2y, vertex2z;
        float uv2U, uv2V;
        float normal2x, normal2y, normal2z;
        unsigned int vertexIndex = vertexIndices[i];
        unsigned int uvIndex = uvIndices[i];
        unsigned int normalIndex = normalIndices[i];

        vertex2x = tempVertices[vertexIndex][0];
        vertex2y = tempVertices[vertexIndex][1];
        vertex2z = tempVertices[vertexIndex][2];
        out_vertices[0].push_back(vertex2x);
        out_vertices[0].push_back(vertex2y);
        out_vertices[0].push_back(vertex2z);

        uv2U = tempUvs[uvIndex][0];
        uv2V = tempUvs[uvIndex][1];
        out_uvs[0].push_back(uv2U);
        out_uvs[0].push_back(uv2V);

        normal2x = tempNormals[normalIndex][0];
        normal2y = tempNormals[normalIndex][1];
        normal2z = tempNormals[normalIndex][2];
        out_normals[0].push_back(normal2x);
        out_normals[0].push_back(normal2y);
        out_normals[0].push_back(normal2z);
    }
}

void ModelWidget::clearModel() {
    for (int i = 0; i < numberOfTextures; ++i) {
        objectVertices[i].clear();
        objectUVs[i].clear();
        objectNormals[i].clear();
        objectVertexIndices[i].clear();
        objectUVIndices[i].clear();
    }

    getOBJData(objStrings[selectedModel].toStdString(), objectVertices[0], objectUVs[0], objectNormals[0],
               objectVertexIndices[0],
               objectUVIndices[0]);
}

bool ModelWidget::getOBJData(std::string fp, std::vector<GLfloat> &out_vertices,
                             std::vector<GLfloat> &out_uvs,
                             std::vector<GLfloat> &out_normals,
                             std::vector<GLuint> &out_vertex_indices,
                             std::vector<GLuint> &out_uv_indices) {
    //Remove whatever is in the buffer
    out_vertices.clear();
    out_uvs.clear();
    out_normals.clear();
    out_vertex_indices.clear();
    out_uv_indices.clear();

    //Face-based indices
    std::vector<GLuint> vertexIndices, uvIndices, normalIndices;

    //Temporary data
    std::vector<std::array<float, 3>> tempVertices;
    std::vector<std::array<float, 2>> tempUvs;
    std::vector<std::array<float, 3>> tempNormals;

    const char *path = fp.c_str();
    FILE *file = fopen(path, "r");
    if (file == NULL) {
        qDebug() << "Cannot open object file !";
        return false;
    }

    ////Loop over all of file until EOF to get of OBJ data
    while (1) {
        char lineHeader[128];
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break;

        ////If vertex data is found
        if (strcmp(lineHeader, "v") == 0) {
            float vertexX;
            float vertexY;
            float vertexZ;
            ///Get vertices and add to temporary vertex array
            fscanf(file, "%f %f %f\n", &vertexX, &vertexY, &vertexZ);
            std::array<float, 3> vertices = {vertexX, vertexY, vertexZ};
            tempVertices.push_back(vertices);
        }
            ////If vertex texture data is found add UV coordinates to temporary UV array
        else if (strcmp(lineHeader, "vt") == 0) {
            float uvU;
            float uvV;
            fscanf(file, "%f %f\n", &uvU, &uvV);
            std::array<float, 2> uv = {uvU, 1.0f - uvV};
            tempUvs.push_back(uv);
        }
            ////If vertex normal data is found add normal data to temporary normal array
        else if (strcmp(lineHeader, "vn") == 0) {
            float normalX;
            float normalY;
            float normalZ;
            fscanf(file, "%f %f %f\n", &normalX, &normalY, &normalZ);
            std::array<float, 3> normals = {normalX, normalY, normalZ};
            tempNormals.push_back(normals);
        }
            ////If face data is found add the 3 vertex indices, 3 UV indices and 3 normal indices to correct arrays.
            ////A face is always made up of triangles as as OBJ file has been triangulated beforehand
        else if (strcmp(lineHeader, "f") == 0) {
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0],
                                 &normalIndex[0],
                                 &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2],
                                 &normalIndex[2]);

            if (matches != 9) {
                qDebug() << "Model is not triangulated";
                return false;
            }
            // 1 is subtracted to facilitate 0 index
            vertexIndices.push_back(vertexIndex[0] - 1);
            vertexIndices.push_back(vertexIndex[1] - 1);
            vertexIndices.push_back(vertexIndex[2] - 1);
            uvIndices.push_back(uvIndex[0] - 1);
            uvIndices.push_back(uvIndex[1] - 1);
            uvIndices.push_back(uvIndex[2] - 1);
            normalIndices.push_back(normalIndex[0] - 1);
            normalIndices.push_back(normalIndex[1] - 1);
            normalIndices.push_back(normalIndex[2] - 1);
        }
    }

    out_uv_indices = uvIndices;

    ////Once all vertex, UV, normal and face data has been obtain sort each data in order stated by face indices
    //Note that ( vertexIndices.size() == uvIndices.size() == normalIndices.size() ) is true if model is triangulated
    for (unsigned int i = 0; i < vertexIndices.size(); i++) {
        unsigned int vertexIndex = vertexIndices[i];
        float vertex2x, vertex2y, vertex2z;
        vertex2x = tempVertices[vertexIndex][0];
        vertex2y = tempVertices[vertexIndex][1];
        vertex2z = tempVertices[vertexIndex][2];
        out_vertices.push_back(vertex2x);
        out_vertices.push_back(vertex2y);
        out_vertices.push_back(vertex2z);

        unsigned int uvIndex = uvIndices[i];
        float uv2U, uv2V;
        uv2U = tempUvs[uvIndex][0];
        uv2V = tempUvs[uvIndex][1];
        out_uvs.push_back(uv2U);
        out_uvs.push_back(uv2V);

        unsigned int normalIndex = normalIndices[i];
        float normal2x, normal2y, normal2z;
        normal2x = tempNormals[normalIndex][0];
        normal2y = tempNormals[normalIndex][1];
        normal2z = tempNormals[normalIndex][2];
        out_normals.push_back(normal2x);
        out_normals.push_back(normal2y);
        out_normals.push_back(normal2z);
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    textureWidget->setUVData(out_uvs, out_uv_indices);
    return true;
}

void ModelWidget::drawObject() {
    glColor3f(1, 1.0, 1.0);

    glDisable(GL_CULL_FACE);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY); //enable normal array

    for (int i = 0; i < numberOfTextures; i++) {
        glPushMatrix();
        if (!styleTransferCheck) {
            glBindTexture(GL_TEXTURE_2D, textureWidget->textureCreator->textures[textureNumber]);
        } else {
            glBindTexture(GL_TEXTURE_2D, textureWidget->textureCreator->textures[13]);
        }

        glVertexPointer(3, GL_FLOAT, 0, objectVertices[i].data());
        glTexCoordPointer(2, GL_FLOAT, 0, objectUVs[i].data());
        glNormalPointer(GL_FLOAT, 0, objectNormals[i].data());
        glDrawArrays(GL_TRIANGLES, 0, objectVertices[i].size() / 3);
        glPopMatrix();
    }

    glDisableClientState(GL_VERTEX_ARRAY); //disable the client states again
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);

    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void ModelWidget::keyPressEvent(QKeyEvent *key) {
    const float cameraSpeed = 0.2f;
    const float turningSpeed = 0.05f;
    //Keep yaw between -pi/2 and pi/2
    if (camX < 0) {
        yaw = (M_PI + yaw);
    }

    //Strife Left
    if (key->key() == Qt::Key_Q) {
        float newYaw = yaw - M_PI / 2;
        cameraPosition[2] += sin(newYaw) * cameraSpeed;
        cameraDirection[2] += sin(newYaw) * cameraSpeed;
        cameraPosition[0] += cos(newYaw) * cameraSpeed;
        cameraDirection[0] += cos(newYaw) * cameraSpeed;
    }
    //Strife Right
    if (key->key() == Qt::Key_E) {
        float newYaw = yaw + M_PI / 2;
        cameraPosition[2] += sin(newYaw) * cameraSpeed;
        cameraDirection[2] += sin(newYaw) * cameraSpeed;
        cameraPosition[0] += cos(newYaw) * cameraSpeed;
        cameraDirection[0] += cos(newYaw) * cameraSpeed;
    }
    //Move Forward
    if (key->key() == Qt::Key_W) {
        cameraPosition[0] += cos(yaw) * cameraSpeed;
        cameraDirection[0] += cos(yaw) * cameraSpeed;
        cameraPosition[2] += sin(yaw) * cameraSpeed;
        cameraDirection[2] += sin(yaw) * cameraSpeed;
    }
    //Move Backwards
    if (key->key() == Qt::Key_S) {
        cameraPosition[0] -= cos(yaw) * cameraSpeed;
        cameraDirection[0] -= cos(yaw) * cameraSpeed;
        cameraPosition[2] -= sin(yaw) * cameraSpeed;
        cameraDirection[2] -= sin(yaw) * cameraSpeed;
    }
    //Turn Left
    if (key->key() == Qt::Key_A) {
        turningNumber += turningSpeed;
    }
    //Turn Right
    if (key->key() == Qt::Key_D) {
        turningNumber -= turningSpeed;
    }

    //Update new direction of camera
    camX = sin(turningNumber) * radius;
    camZ = cos(turningNumber) * radius;
    cameraDirection[0] = cameraPosition[0] - camX;
    cameraDirection[2] = cameraPosition[2] - camZ;
    if (camX != 0) {
        yaw = atan(camZ / camX);
    }

    //Pan up
    if (key->key() == Qt::Key_R) {
        cameraPosition[1] += 0.2;
        cameraDirection[1] += 0.2;
    }
    //Pan Down
    if (key->key() == Qt::Key_F) {
        cameraPosition[1] -= 0.2;
        cameraDirection[1] -= 0.2;
    }
    //Tilt Up
    if (key->key() == Qt::Key_T) {
        cameraDirection[1] -= 0.02;
    }
    //Tilt Down
    if (key->key() == Qt::Key_G) {
        cameraDirection[1] += 0.02;
    }
}

void ModelWidget::paintGL() { // paintGL()
//    Draws model in widget
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.251, 0.251, 0.251, 1);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);

    glColor3f(1, 1, 1);
    glPushMatrix();
    glScalef(0.5, 0.5, 0.5);
    drawObject();
    glPopMatrix();
    glLoadIdentity();
    gluLookAt(cameraDirection[0], cameraDirection[1], cameraDirection[2], cameraPosition[0], cameraPosition[1],
              cameraPosition[2], cameraUp[0], cameraUp[1] + 10000000000000, cameraUp[2]);
    glFlush();
}

void ModelWidget::changeTextureNumber(int i) {
    textureNumber = i + 2;
}
