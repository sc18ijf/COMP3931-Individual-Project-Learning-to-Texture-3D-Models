#ifndef SCENE_TextureCreator_H
#define SCENE_TextureCreator_H

#include <cmath>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <GL/glu.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <QtCore/QTime>
#include <QGLWidget>
#include "qopenglfunctions.h"
#include <QOpenGLTexture>
#include <QtGui>
#include <QtCore/QTime>
#include <string>

using namespace std;

class TextureCreator {
public:
    TextureCreator();

    void imageLoader(QStringList sImage, GLuint texture[]);

    string modelName = "model9";

    //Texture relative file names
    QStringList textureStrings = {
            "../Textures/UVMap2.png", //0
            "./results/model8/test_latest/images/facade_0_fake_B.png",
            "../results/val/images/input_000_random_sample01.png",
            "../results/val/images/input_000_random_sample02.png",
            "../results/val/images/input_000_random_sample03.png",
            "../results/val/images/input_000_random_sample04.png",
            "../results/val/images/input_000_random_sample05.png",
            "../results/val/images/input_000_random_sample06.png",
            "../results/val/images/input_000_random_sample07.png",
            "../results/val/images/input_000_random_sample08.png",
            "../results/val/images/input_000_random_sample09.png",
            "../results/val/images/input_000_random_sample10.png",
            "../results/val/images/input_000_random_sample11.png",
            "./results/facades_Add5_10/test_latest/images/style_GAN_mix.png",
            "./results/facades_Add5_10/test_latest/images/style.png",
    };

    QStringList folderTextureNames = {
            "wood",
            "metal",
            "foliage",
            "stone",
            "banded",
            "chequered",
            "dotted",
            "scaly",
            "woven",
            "zigzagged",
    };

    //Index of each image
    GLuint UVMapIndex = 0;
    int selectedUV = 0;

    static constexpr int constNumberOfTextures = 4 + 10 ;//3 + 100 + 100 + 1;
    int numberOfTextures;
    GLuint textures[constNumberOfTextures];

protected:

private:

    void stringAppender();

};

#endif //SCENE_TextureCreator_H
