#include "TextureWidget.h"

////Constructor
TextureWidget::TextureWidget(QWidget *parent) : QGLWidget(parent) { // constructor
    textureCreator = new TextureCreator();
    qPen.setWidth(1.5);
    makeTriangles();
    makeScene();
    const QRect qRect = QRect(0, 0, width, height);

    //set QGraphicsScene Settings
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scene->setSceneRect(qRect);
    view->setScene(scene);
    view->setGeometry(QRect(-1, -1, width + 1, height + 1));
    view->setStyleSheet("border: 0px");
    view->setMouseTracking(true);
    view->show();
    view->viewport()->installEventFilter(this);
}

// Event handler for QGraphicsScene
bool TextureWidget::eventFilter(QObject *obj, QEvent *event) {
    if (obj == view->viewport()) {
        // timer
        t1 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        if (event->type() == QEvent::MouseButtonRelease) {
            // Release mouse button event
            mouseDown = false;
        } else if (event->type() == QEvent::MouseMove) {
            // Move mouse event

            // Moves brushes position
            QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent *>(event);
            brushPosition[0] = mouseEvent->localPos().x() - brushRadius;
            brushPosition[1] = mouseEvent->localPos().y() - brushRadius;

            //Add triangles to filled triangles array
            if (mouseDown) {//&& triangleAddReady) {
                addTriangles(mouseEvent);
            }

            //Repaint
            this->update();
            return true;

        } else if (event->type() == QEvent::MouseButtonPress) {
            // Set mouse to be pressed and looks circle triangle collisions
            mouseDown = true;
            QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent *>(event);
            addTriangles(mouseEvent);
        } else {
            return false;
        }
    }
    return false;
}


////Slots
void TextureWidget::changeBrushSize(int brushSize) {
    //1 to 100
    float brushSizeFloat = brushSize + 1;
    brushRadius = brushSizeFloat;

    if (showUVOption) {
        //Paint new brush
        scene->removeItem(pCircleBrush);
        QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(brushPosition[0] - brushRadius,
                                                                brushPosition[1] - brushRadius, brushRadius * 2,
                                                                brushRadius * 2);
        pCircleBrush = circle;
        scene->addItem(pCircleBrush);
    }

    this->update();
}

void TextureWidget::setFixedSizeSlot() {
    // Sets the texture widget to 512x512
    if (width != facadeRes && height != facadeRes) {
        tempGlobalWidth = width;
        tempGlobalHeight = height;
    }
    this->resize(facadeRes, facadeRes);
}

void TextureWidget::setUnfixedSizeSlot() {
    // Allow texture widget to expand
    this->resize(tempGlobalWidth, tempGlobalHeight);
}

void TextureWidget::changeStartingNumber(QString startingNumber) {
    int start = startingNumber.toInt();
    startNum = start;
}

void TextureWidget::changePaddingNumber(QString paddingNumber) {
    padding = paddingNumber.toInt();
}

void TextureWidget::changeSemanticColour(int i) {
    semanticColour = i;
}

void TextureWidget::changeUVMap(int i) {
    selectedUV = i + 1;
    filledTriangles.clear();
    makeTriangles();
    makeScene();
    this->update();
}

void TextureWidget::generateFacades() {
    QImage *wholeFacade = new QImage(facadeRes * 2, facadeRes, QImage::Format_RGB32);

    showUVMapBackground = false;
    this->showUVMap(0);
    bool tempUVOption = showUVOption;
    int tempWidth = width;
    int tempHeight = height;

//    Creates image from QWidget
    this->resizeGL(512, 512);
    QRect rect = QRect(0, 0, width, height);
    QPixmap qPixmap(rect.size());
    this->render(&qPixmap, QPoint(), QRegion(rect));
    QImage textureWidgetImage(qPixmap.toImage());
    textureWidgetImage = textureWidgetImage.scaled(facadeRes, facadeRes, Qt::IgnoreAspectRatio, Qt::FastTransformation);

//  Array of colours in image
    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            QColor color(textureWidgetImage.pixel(i, j));
            textureWidgetColorArray[i][j] = color;
        }
    }

//    Gets all surrounding colours in padding distance and changes selected pixel to most common colour
    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            vector <QColor> surroundingColors;
            vector<int> surroundingColorsOccurrences;

            for (int l = i - padding; l <= i + padding; l++) {
                for (int m = j - padding; m <= j + padding; m++) {
                    if (l <= facadeRes - 1 && l >= 0 && m <= facadeRes - 1 && m >= 0 && m != j && l != i &&
                        textureWidgetColorArray[l][m] != QColor(255, 255, 255)) {
                        surroundingColors.push_back(textureWidgetColorArray[l][m]);

                    }
                }
            }

            if (!surroundingColors.empty()) {
                std::map<int, int> counters;
                if (textureWidgetColorArray[i][j] == QColor(255, 255, 255) && !surroundingColors.empty()) {
                    for (auto const &k: surroundingColors) {
                        int cInt = colourMatchReturnColourInt(k);
                        counters[cInt]++;
                    }
                }

                int currentMax = 0;
                int arg_max = 0;
                for (auto it = counters.cbegin(); it != counters.cend(); ++it) {
                    if (it->second > currentMax) {
                        arg_max = it->first;
                        currentMax = it->second;
                    }
                }
                QColor color = semanticColourStrings[arg_max];
                textureWidgetColorArray2[i][j] = color;
            } else {
                textureWidgetColorArray2[i][j] = textureWidgetColorArray[i][j];
            }
        }
    }

    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            if (textureWidgetColorArray[i][j] != QColor(Qt::white)) {
                textureWidgetColorArray2[i][j] = textureWidgetColorArray[i][j];
            }
        }
    }

    qDebug() << "creating dir";

    fs::create_directory(pathString);

    qDebug() << "creating facade";

    for (int i = 0 + startNum; i < numberOfFacades + startNum; i++) {

        array<int, TextureWidget::numberOfTextures> randomArray = getRandomIntArray(i);

        std::vector <QImage> textures;
        for (int j = 0; j < numberOfTextures; j++) {
            QImage someimg = QImage(textureCreator->textureStrings[randomArray[j]]);
            if (someimg.isNull())
                //TODO reduce images created to only ones needed
                textures.push_back(QImage(textureCreator->textureStrings[randomArray[j]]).scaled(facadeRes, facadeRes,
                                                                                                 Qt::IgnoreAspectRatio,
                                                                                                 Qt::FastTransformation));
        }


        for (int j = 0; j < facadeRes; j++) {
            for (int k = 0; k < facadeRes; k++) {
                //if pixel is white
                if (textureWidgetColorArray2[j][k] == (QColor(255, 255, 255))) {
                    wholeFacade->setPixelColor(j, k, QColor(255, 255, 255));
                    wholeFacade->setPixelColor(j + facadeRes, k, QColor(255, 255, 255));
                } else {
                    if (colourMatch(textureWidgetColorArray2[j][k])) {
                        int cInt = colourMatchReturnColourInt(textureWidgetColorArray2[j][k]);
                        if (cInt != -1) {
                            wholeFacade->setPixelColor(j, k, textures[cInt].pixelColor(j, k));
                        } else {
                            wholeFacade->setPixelColor(j, k, QColor("black"));
                        }
                    } else {
                        wholeFacade->setPixelColor(j, k, QColor("black"));

                    }
                }
                //set semantic colour
                wholeFacade->setPixelColor(j + facadeRes, k, textureWidgetColorArray2[j][k]);
            }
        }
        QString number = QString::number(i);
        qDebug() << "saving " + number;

        wholeFacade->save(QString::fromStdString(pathString) + "/facade_" + number + ".jpg", nullptr, 100);
    }
    showUVMapBackground = true;
    showUVOption = tempUVOption;
    this->showUVMap(2);
}

////OpenGL
void TextureWidget::initializeGL() {
    // Set the widget background colour
    glClearColor(0.8, 0.3, 0.3, 0.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    makeTriangles();
//    makeScene();

}

void TextureWidget::resizeGL(int w, int h) { // resizeGL()
    // Recalculates triangles and stretches scene to fit widget on a resize event
    if (w < h) {
        h = w;
    } else {
        w = h;
    }
    width = w;
    height = h;
    this->resize(width, height);
    makeTriangles();
    scene->setSceneRect(0, 0, w, h);
    view->setGeometry(QRect(0, 0, width, height));
    scene->clear();
    if (showUVOption) {
        remakeScene();
    } else {
        redrawFilledTrianglesNoEdges(filledTriangles);
    }
}

void TextureWidget::setUVData(std::vector <GLfloat> &uvs, std::vector <GLuint> &indices) {
//    Changes uv data
    objectUVs = uvs;
    objectUVIndices = indices;
}

void TextureWidget::drawEmptyTriangles() {
    pTriangleLines.clear();
    for (int i = 0; i < objectUVs.size(); i += 6) {
        int uCoord1 = objectUVs[i] * width;
        int vCoord1 = objectUVs[i + 1] * height;
        int uCoord2 = objectUVs[i + 2] * width;
        int vCoord2 = objectUVs[i + 3] * height;
        int uCoord3 = objectUVs[i + 4] * width;
        int vCoord3 = objectUVs[i + 5] * height;

        if (showUVOption) {
            QGraphicsLineItem *line1 = scene->addLine(uCoord1, vCoord1, uCoord2, vCoord2);
            QGraphicsLineItem *line2 = scene->addLine(uCoord2, vCoord2, uCoord3, vCoord3);
            QGraphicsLineItem *line3 = scene->addLine(uCoord3, vCoord3, uCoord1, vCoord1);

            pTriangleLines.push_back(line1);
            pTriangleLines.push_back(line2);
            pTriangleLines.push_back(line3);
        }
    }
}

void TextureWidget::makeTriangles() {
//    Calculates triangles positions based on UV data
    triangles.clear();
    for (int i = 0; i < objectUVs.size(); i += 6) {
        int uCoord1 = objectUVs[i] * width;
        int vCoord1 = objectUVs[i + 1] * height;
        int uCoord2 = objectUVs[i + 2] * width;
        int vCoord2 = objectUVs[i + 3] * height;
        int uCoord3 = objectUVs[i + 4] * width;
        int vCoord3 = objectUVs[i + 5] * height;

        std::array<int, 6> triangle = {uCoord1,
                                       vCoord1,
                                       uCoord2,
                                       vCoord2,
                                       uCoord3,
                                       vCoord3};
        triangles.push_back(triangle);
    }
}


void TextureWidget::makeScene() {
    scene->clear();
    drawNewFilledTriangles(filledTriangles);
    drawEmptyTriangles();
    QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(brushPosition[0] - brushRadius,
                                                            brushPosition[1] - brushRadius, brushRadius * 2,
                                                            brushRadius * 2);
    pCircleBrush = circle;//brushPosition[0],brushPosition[1], brushRadius * 2, brushRadius * 2);
    scene->addItem(pCircleBrush);
}

void TextureWidget::remakeScene() {
    scene->clear();
    redrawFilledTrianglesWithEdges(filledTriangles);
    drawEmptyTriangles();
    QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(brushPosition[0] - brushRadius,
                                                            brushPosition[1] - brushRadius, brushRadius * 2,
                                                            brushRadius * 2);
    pCircleBrush = circle;
    scene->addItem(pCircleBrush);
}

void TextureWidget::remakeSceneNoUVMapOrCircle() {
    scene->clear();
    redrawFilledTrianglesNoEdges(filledTriangles);
    this->update();
}


void TextureWidget::paintEvent(QPaintEvent *event) {

    const QRect qRect(QPoint(0, 0), QSize(width, height));
    if (showUVMapBackground) {
//        painter.drawImage(qRect, QImage(textureCreator->textureStrings[0]));
    }

    filledTrianglesSize = filledTriangles.size();
    if (showUVOption) {
        if (filledTrianglesSize != filledTrianglesSizeTemp) {
            drawNewFilledTriangles(filledTriangles);
        }
        filledTrianglesSizeTemp = filledTrianglesSize;

        scene->removeItem(pCircleBrush);
        QGraphicsEllipseItem *circle = new QGraphicsEllipseItem(brushPosition[0],
                                                                brushPosition[1], brushRadius * 2,
                                                                brushRadius * 2);
        pCircleBrush = circle;
        scene->addItem(pCircleBrush);
    }


    t2 = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    uint64_t t3 = t2 - t1;
//    Finish Timer at end of paint event
//    qDebug() << t3<< " ms between frames"; //<< t2 << t1;
}


void TextureWidget::addTriangles(QMouseEvent *event) {
    triangleAddReady = false;
    int triangleNumber = -1;
    int colour = semanticColour;
    //If mouse is clicked and mouse moves check to see if inside a UV triangle
    int x = (event->localPos().x());
    int y = (event->localPos().y());

    for (int i = 0; i < triangles.size(); i++) {
        bool in = false;
        //Check circle is in any triangle
        in = isInside(triangles[i][0], triangles[i][1], triangles[i][2], triangles[i][3],
                      triangles[i][4],
                      triangles[i][5], x, y, brushRadius);

        //Triangle found
        if (in) {
            triangleNumber = i;
            std::array<int, 2> triangleData = {
                    triangleNumber,
                    colour
            };


            int j;
            for (j = 0; j < filledTriangles.size(); j++) {
                //If the new triangle is already in filledTriangle array
                if (filledTriangles[j][0] == triangleData[0]) {
                    //if the new colour is different change the colour
                    if (filledTriangles[j][1] != triangleData[1]) {
                        //change colour
                        filledTriangles[j][1] = triangleData[1];
                    } else {
                        //otherwise ignore it
                        break;
                    }
                }
            }

            //if the triangle isn't in the filledTriangle array add the new triangle to it and repaint
            if (j == filledTriangles.size()) {
                filledTriangles.push_back(triangleData);
                redrawUVMap = true;
                this->update();
            }
        }
    }
    triangleAddReady = true;
}


void TextureWidget::clearFilledTriangles() {
    scene->clear();
    filledTriangles.clear();
    drawEmptyTriangles();
    this->update();
}

void TextureWidget::drawNewFilledTriangles(std::vector <std::array<int, 2>> triangleData) {
//    Adds a new triangle to the list of filled triangles
    for (int i = filledTrianglesSizeTemp; i < filledTrianglesSize; i++) {
        int triangleNumber = triangleData[i][0];
        int colourIndex = triangleData[i][1];
        qBrush = QBrush(QColor(semanticColourStrings[colourIndex]));
        QPolygonF tri;
        QPointF p1 = QPointF(triangles[triangleNumber][0], triangles[triangleNumber][1]);
        QPointF p2 = QPointF(triangles[triangleNumber][2], triangles[triangleNumber][3]);
        QPointF p3 = QPointF(triangles[triangleNumber][4], triangles[triangleNumber][5]);
        tri << p1 << p2 << p3;

        QGraphicsPolygonItem *pTriangleItem = scene->addPolygon(tri, qPen, qBrush);

        pTriangleItems.push_back(pTriangleItem);
    }
}

void TextureWidget::redrawFilledTrianglesWithEdges(std::vector <std::array<int, 2>> triangleData) {
//    Redraws all triangles
    pTriangleItems.clear();
    for (int i = 0; i < filledTriangles.size(); i++) {
        int triangleNumber = triangleData[i][0];
        int colourIndex = triangleData[i][1];
        qBrush = QBrush(QColor(semanticColourStrings[colourIndex]));
        QPolygonF tri;
        QPointF p1 = QPointF(triangles[triangleNumber][0], triangles[triangleNumber][1]);
        QPointF p2 = QPointF(triangles[triangleNumber][2], triangles[triangleNumber][3]);
        QPointF p3 = QPointF(triangles[triangleNumber][4], triangles[triangleNumber][5]);
        tri << p1 << p2 << p3;
        QGraphicsPolygonItem *pTriangleItem = scene->addPolygon(tri, qPen, qBrush);
        pTriangleItems.push_back(pTriangleItem);
    }
}

void TextureWidget::redrawFilledTrianglesNoEdges(std::vector <std::array<int, 2>> triangleData) {
//    Redraws all triangles with no outline
    pTriangleItems.clear();
    for (int i = 0; i < filledTriangles.size(); i++) {
        int triangleNumber = triangleData[i][0];
        int colourIndex = triangleData[i][1];
        qBrush = QBrush(QColor(semanticColourStrings[colourIndex]));
        QPolygonF tri;
        QPointF p1 = QPointF(triangles[triangleNumber][0], triangles[triangleNumber][1]);
        QPointF p2 = QPointF(triangles[triangleNumber][2], triangles[triangleNumber][3]);
        QPointF p3 = QPointF(triangles[triangleNumber][4], triangles[triangleNumber][5]);
        tri << p1 << p2 << p3;

        qPen.setWidth(0);
        qPen.setColor(semanticColourStrings[colourIndex]);
        QGraphicsPolygonItem *pTriangleItem = scene->addPolygon(tri, qPen, qBrush);
        qPen.setColor(Qt::black);
        qPen.setWidth(1.5);

        pTriangleItems.push_back(pTriangleItem);
    }
}


void TextureWidget::showUVMap(int i) {
    if (i == 0) {
        showUVOption = false;
        scene->clear();
        redrawFilledTrianglesNoEdges(filledTriangles);
    } else {
        showUVOption = true;
        scene->clear();
        remakeScene();
    }
    this->update();
}

////Tri-circle intersections
bool TextureWidget::isInside(float x1, float y1, float x2, float y2, float x3, float y3, float uLocation,
                             float vLocation, float radius) {
    if ((x1 == x2 && x1 == x3 && y1 == y2 && y1 == y3))
        return false;

    float dist1X = x1 - uLocation;
    float dist1Y = y1 - vLocation;
    float distanceFromCircle1 = sqrt(pow(dist1X, 2) + pow(dist1Y, 2));
    if (distanceFromCircle1 <= radius)
        return true;


    float dist2X = x2 - uLocation;
    float dist2Y = y2 - vLocation;
    float distanceFromCircle2 = sqrt(pow(dist2X, 2) + pow(dist2Y, 2));
    if (distanceFromCircle2 <= radius)
        return true;


    float dist3X = x3 - uLocation;
    float dist3Y = y3 - vLocation;
    float distanceFromCircle3 = sqrt(pow(dist3X, 2) + pow(dist3Y, 2));
    if (distanceFromCircle3 <= radius)
        return true;

    //AB
    float v1X = x2 - x1;
    float v1Y = y2 - y1;
    //AC
    float v2X = x3 - x1;
    float v2Y = y3 - y1;
    //AP
    float v3X = uLocation - x1;
    float v3Y = vLocation - y1;

    float determinant = v1X * v2Y - (v2X * v1Y);
    //if determinant of the two vectors is less than 0 then the triangle is clockwise and should be reordered. If the det is 0 then co-linear
    if (determinant < 0) {
        float tempX = x2;
        float tempY = y2;
        x2 = x3;
        y2 = y3;
        x3 = tempX;
        y3 = tempY;
    }

    //Vectors AB, BC, CA
    float AB[2] = {x2 - x1, y2 - y1};
    float BC[2] = {x3 - x2, y3 - y2};
    float CA[2] = {x1 - x3, y1 - y3};


    float denominator = v1X * v2Y - v2X * v1Y;
    float v = (v3X * v2Y - v2X * v3Y) / denominator;
    float w = (v1X * v3Y - v3X * v1Y) / denominator;
    float u = 1.0f - v - w;

    if (v > 0 && w > 0 && u > 0) {
        return true;
    }

    bool ABIntersection = circleLineSegCollision(x1, y1, x2, y2, uLocation, vLocation, radius);
    if (ABIntersection)
        return true;

    bool BCIntersection = circleLineSegCollision(x2, y2, x3, y3, uLocation, vLocation, radius);
    if (BCIntersection)
        return true;

    bool CAIntersection = circleLineSegCollision(x3, y3, x1, y1, uLocation, vLocation, radius);
    if (CAIntersection)
        return true;

    return false;
}

bool TextureWidget::circleLineSegCollision(float x1, float y1, float x2, float y2, float x, float y, float radius) {
    // Implementation of https://mathworld.wolfram.com/Circle-LineIntersection.html
    //Center around circles origin
    x1 = x1 - x;
    y1 = y1 - y;
    x2 = x2 - x;
    y2 = y2 - y;

    float AB[2] = {x2 - x1, y2 - y1};
    float ABr = sqrt(AB[0] * AB[0] + AB[1] * AB[1]);

    float determinant = x1 * y2 - x2 * y1;
    float discriminant = radius * radius * ABr * ABr - determinant * determinant;

    if (discriminant >= 0) {
        float xPlus = ((determinant * AB[1]) + (sign(AB[1]) * sqrt(discriminant))) / (ABr * ABr);
        float xMinus = ((determinant * AB[1]) - (sign(AB[1]) * sqrt(discriminant))) / (ABr * ABr);
        float yPlus = ((-1 * determinant * AB[0]) + (sign(AB[1]) * sqrt(discriminant))) / (ABr * ABr);
        float yMinus = ((-1 * determinant * AB[0]) - (sign(AB[1]) * sqrt(discriminant))) / (ABr * ABr);

        if (x1 > x2) {
            if (y1 > y2) {
                if ((xPlus < x1 && yPlus < y1 && xPlus > x2 && yPlus > y2) ||
                    (xMinus < x1 && yMinus < y1 && xMinus > x2 && yMinus > y2))
                    return true;
            } else {
                if ((xPlus < x1 && yPlus > y1 && xPlus > x2 && yPlus < y2) ||
                    (xMinus < x1 && yMinus > y1 && xMinus > x2 && yMinus < y2))
                    return true;
            }
        } else {
            if (y1 > y2) {
                if ((xPlus > x1 && yPlus < y1 && xPlus < x2 && yPlus > y2) ||
                    (xMinus > x1 && yMinus < y1 && xMinus < x2 && yMinus > y2))
                    return true;
            } else {
                if ((xPlus > x1 && yPlus > y1 && xPlus < x2 && yPlus < y2) ||
                    (xMinus > x1 && yMinus > y1 && xMinus < x2 && yMinus < y2))
                    return true;
            }
        }

        if (x1 == x2)
            if (y1 > y2) {
                return (yPlus < y1 && yPlus > y2) || (yMinus < y1 && yMinus > y2);
            } else {
                return (yPlus > y1 && yPlus < y2) || (yMinus > y1 && yMinus < y2);
            }

        if (y1 == y2)
            if (x1 > x2) {
                return ((xPlus < x1 && xPlus > x2) || (xMinus < x1 && xMinus > x2));
            } else {
                return ((xPlus > x1 && xPlus < x2) || (xMinus > x1 && xMinus < x2));
            }
    }
    return false;
}

int TextureWidget::sign(int x) {
    if (x < 0) {
        return -1;
    } else {
        return 1;
    }
}

////Facade Generation
bool TextureWidget::colourMatch(QColor pixelColor) {
    for (int i = 0; i < numberOfTextures; i++) {
        if (pixelColor == QColor(semanticColourStrings[i])) {
            return true;
        }
    }
    return false;
}

int TextureWidget::colourMatchReturnColourInt(QColor pixelColor) {
    for (int i = 0; i < numberOfTextures; i++) {
        if (pixelColor == QColor(semanticColourStrings[i])) {
            return i;
        }
    }
    return -1;
}

array<int, TextureWidget::numberOfTextures> TextureWidget::getRandomIntArray(int i) {
//    Creates an array on random numbers that can be used to create texture images
    int offset = 15;
    srand(i);
    //TODO For loop of rand ints
    int randIntWood = (rand() % 100) + (offset);
    int randIntMetal = (rand() % 100) + (offset + 100 * 1);
    int randIntFoliage = (rand() % 100) + (offset + 100 * 2);
    int randIntStone = (rand() % 100) + (offset + 100 * 3);
    int randIntBanded = (rand() % 120) + (offset + 100 * 4);
    int randIntChequered = (rand() % 120) + (offset + 100 * 4) + (120 * 1);
    int randIntDotted = (rand() % 120) + (offset + 100 * 4) + (120 * 2);
    int randIntScaly = (rand() % 120) + (offset + 100 * 4) + (120 * 3);
    int randIntWoven = (rand() % 120) + (offset + 100 * 4) + (120 * 4);
    int randIntZigzagged = (rand() % 120) + (offset + 100 * 4) + (120 * 5);
    array<int, TextureWidget::numberOfTextures> randomArray = {randIntWood,
                                                               randIntMetal,
                                                               randIntFoliage,
                                                               randIntStone,
                                                               randIntBanded,
                                                               randIntChequered,
                                                               randIntDotted,
                                                               randIntScaly,
                                                               randIntWoven,
                                                               randIntZigzagged,
    };
    return randomArray;
}


void TextureWidget::generateTestFacade() {
//    Create a new semantic map
    QImage *wholeFacade = new QImage(facadeRes * 2, facadeRes, QImage::Format_RGB32);
    QImage *wholeFacadePadded = new QImage(facadeRes * 2, facadeRes, QImage::Format_RGB32);

    showUVMapBackground = false;
    this->showUVMap(0);
    bool tempUVOption = showUVOption;
    int tempWidth = width;
    int tempHeight = height;


    QRect rect = QRect(0, 0, width, height);
    QPixmap qPixmap(rect.size());
    this->render(&qPixmap, QPoint(), QRegion(rect));
    QImage textureWidgetImage(qPixmap.toImage());
    textureWidgetImage = textureWidgetImage.scaled(facadeRes, facadeRes, Qt::IgnoreAspectRatio, Qt::FastTransformation);

    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            QColor color(textureWidgetImage.pixel(i, j));
            textureWidgetColorArray[i][j] = color;
        }
    }

    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            vector <QColor> surroundingColors;
            vector<int> surroundingColorsOccurrences;
            for (int l = i - padding; l <= i + padding; l++) {
                for (int m = j - padding; m <= j + padding; m++) {
                    if (l <= facadeRes - 1 && l >= 0 && m <= facadeRes - 1 && m >= 0 && m != j && l != i &&
                        textureWidgetColorArray[l][m] != QColor(255, 255, 255)) {
                        surroundingColors.push_back(textureWidgetColorArray[l][m]);
                    }
                }
            }

            if (!surroundingColors.empty()) {
                std::map<int, int> counters;
                if (textureWidgetColorArray[i][j] == QColor(255, 255, 255) && !surroundingColors.empty()) {
                    for (auto const &k: surroundingColors) {
                        int cInt = colourMatchReturnColourInt(k);
                        counters[cInt]++;
                    }
                }

                int currentMax = 0;
                int arg_max = 0;
                for (auto it = counters.cbegin(); it != counters.cend(); it++) {
                    if (it->second > currentMax) {
                        arg_max = it->first;
                        currentMax = it->second;
                    }
                }

                QColor color = semanticColourStrings[arg_max];
                textureWidgetColorArray2[i][j] = color;

            } else {
                textureWidgetColorArray2[i][j] = textureWidgetColorArray[i][j];
            }
        }
    }

    for (int i = 0; i < facadeRes; i++) {
        for (int j = 0; j < facadeRes; j++) {
            if (textureWidgetColorArray[i][j] != QColor(Qt::white)) {
                textureWidgetColorArray2[i][j] = textureWidgetColorArray[i][j];
            }
        }
    }

    array<int, TextureWidget::numberOfTextures> randomArray = getRandomIntArray(0);
    std::vector <QImage> textures;
    for (
            int j = 0;
            j < numberOfTextures;
            j++) {
        QImage someimg = QImage(textureCreator->textureStrings[randomArray[j]]);

        if (someimg.isNull())
            qDebug() << textureCreator->folderTextureNames[j] << "missing";
        //TODO reduce images created to only ones needed

        textures.push_back(QImage(textureCreator->textureStrings[randomArray[j]]).scaled(facadeRes, facadeRes,
                                                                                         Qt::IgnoreAspectRatio,
                                                                                         Qt::FastTransformation));
    }

    for (int j = 0; j < facadeRes; j++) {
        for (int k = 0; k < facadeRes; k++) {
            //if pixel is white
            if (textureWidgetColorArray2[j][k] == (QColor(255, 255, 255))) {
                wholeFacade->setPixelColor(j, k, QColor(255, 255, 255));
                wholeFacade->setPixelColor(j + facadeRes, k, QColor(255, 255, 255));
            } else {
                if (colourMatch(textureWidgetColorArray2[j][k])) {
                    int cInt = colourMatchReturnColourInt(textureWidgetColorArray2[j][k]);
                    if (cInt != -1) {
                        wholeFacade->setPixelColor(j, k, textures[cInt].pixelColor(j, k));
                    } else {
                        wholeFacade->setPixelColor(j, k, QColor("black"));
                    }
                } else {
                    wholeFacade->setPixelColor(j, k, QColor("black"));
                }
            }
            //set semantic colour
            wholeFacade->setPixelColor(j + facadeRes, k, textureWidgetColorArray2[j][k]);
        }
    }


    fs::create_directory("../GAN_Model/fullModel/datasets/add5Examples/");
    fs::create_directory("../GAN_Model/fullModel/datasets/add5Examples/test/");

    fs::create_directory("../GAN_Model/fullModel/datasets/add5Examples/");
    fs::create_directory("../GAN_Model/fullModel/datasets/add5Examples/val/");
    fs::create_directory("../GAN_Model/fullModel/datasets/add5Examples/val/");
    fs::create_directory("../eval/");

    wholeFacade->save(QString::fromStdString(pathTestString), nullptr, 100);
    wholeFacade->save(QString::fromStdString(pathTestStringBicycleGAN), nullptr, 100);
    wholeFacade->save(QString::fromStdString("../eval/"), nullptr, 100);
    showUVMapBackground = true;
    showUVOption = tempUVOption;
    this->showUVMap(2);
}
